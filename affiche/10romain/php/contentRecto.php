

<div class="contentcontent">

	<div class="titre"><span>Poisson-Évêque</span><sup>Festival</sup>⟶ 21 au 30 avril 2017</div>
<!--
	<div class="date">

	</div> -->


	<div class="people">
		<!-- <p class="curatrice"><b>Curatrice&thinsp;:</b>
			Un festival organisé par Stéphanie Vérin.
		</p> -->
		<ul><li class="list1"><span>Marjolein</span><sup>Kitchen</sup> <span>Guldentops</span></li>
			<li class="list2"><span>Stéphanie</span> <sup>Perfomance</sup><span>Becquet</span></li>
			<li class="list3"><span>Stephan</span><sup>Balade</sup> <span>Blumenschein</span></li>
			<li class="list4"><span>Otto</span> <sup>Performance</sup><span>Byström</span></li>
			<li class="list5"><span>André</span><sup>Concert</sup> <span>D.Chapatte</span></li>
			<li class="list6"><span>Brique</span> <sup>Concert</sup><span>Corps</span></li>
			<li class="list7"><span>Rares</span><sup>Performance</sup> <span>Craiut</span></li>
			<li class="list8"><span>Jot</span> <sup>Vernissage</sup><span>Fau</span></li>
			<li class="list9"><span>Irina</span><sup>Performance</sup> <span>Favero-Longo</span></li>
			<li class="list10"><span>Xavier</span> <sup>Performance</sup><span>Gorgol</span></li>
			<li class="list11"><span>Marine</span><sup>Balade</sup> <span>Kaiser</span></li>
			<li class="list12"><span>Maxime</span> <sup>Concert</sup><span>Lacôme</span></li>
			<li class="list13"><span>Antoine</span><sup>Récit</sup> <span>Loyer</span></li>
			<li class="list14"><span>Sandra</span> <sup>Récit</sup><span>Naji</span></li>
			<li class="list15"><span>Clément</span><sup>Balade</sup> <span>Thiry</span></li>
			<li class="list16"><span>Rebecca</span> <sup>Performance</sup><span>Konforti</span></li>
			<li class="list17" style="text-align:left;"><span>Potential<sup>Balade</sup> Office&thinsp;Project</span></span></li>
				<li class="list19"><span>Stéphanie</span> <sup>Balade</sup><span>Verin</span></li>
			<li class="list18"><span>Bettina</span><sup>Chronique</sup> <span>Kordjani</span></li>
			<li class="list20"><span>Romy</span> <sup>Kitchen</sup><span>Vanderveken</span></li>
		</ul>
	</div>

	<p class="lieu">
		<span>Le Maga</span>
		<span>Avenue Jean Volders, 56</span><br>
		</p>
	<p class="lieu">
		<span> Bar Waterloo</span>
		<span>Chaussée de Waterloo, 217</span><br>
	
	</p>
	<p class="lieu">
		<span> Snack ATLAS</span>
		<span>Parvis de la Trinité, 6</span><br>
	
	</p>

<!--	<?php include('texte-steph.php') ?>-->

</div>

<div class="marge margeHoriz margeTop"></div>
<div class="marge margeHoriz margeBottom"></div>
<div class="marge margeVert margeLeft"></div>
<div class="marge margeVert margeRight"></div>

<!-- <div id="gridVisible">
	<div class="rows">
		<div class="line horizontal row"></div>
	</div>
	<div class="cols">
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
	</div>
</div> -->

<!-- <img class="bgOr" src="img/Hobos2.jpg" alt="" /> -->

<div class="rectangle"></div>
