<div class="texteSteph">
  Le festival Poisson-Evêque est une première édition regroupant des artistes de Finlande, de Suisse,
de France, de Hollande, de Belgique et de Roumanie ayant une pratique de la sculpture, de la
performance et de la ballade. Les actions proposés questionneront la gentrification, les féminismes
émergeants, les effets sociaux du capitalisme et les manières de se réapproprier les outils techniques
et technologiques contemporains. Les événements du festival se dérouleront au Maga, lieu
associatif situé dans le quartier très populaire de Saint-Gilles mais aussi dans des snacks, des bars et
dans la ville de Bruxelles, afin de proposer une forme d'art social et utopiste engageant un dialogue
avec la population présente.
</div>
