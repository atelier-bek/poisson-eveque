<div id="eveque">

  <div class="col1">

<h7>Festival</h7>&nbsp;&nbsp;&nbsp;<em>Poisson―Évêque</em>Poisson―Évêque</h5>
    <b>21 — 30 <h7>avril 2017</h7><br></b><br>
    
<p>Le <b>festival</b> Poisson-Évêque est une première édition regroupant vingt <b>artistes </b> internationaux. Pendant dix jours, 
nous proposerons une <b>exposition </b> , des récits musicaux, des lectures, des performances, des balades dans la ville, 
mais aussi des concerts. Nous questionnerons ensemble la gentri&shy;fication, les féminismes émergeants, les effets sociaux du capitalisme 
et les manières de se réapproprier les outils techniques et technologiques contemporains. Vous serez conviés à découvrir les coins sombres 
de la ville de <b>Bruxelles </b>, à discuter autour de madeleines queer, à rencontrer nos faiseuses de soupe dans un appartement Saint-Gillois, à participer 
à des lectures publiques de bar en bar, à observer les sculptures de femmes et l’aménagement urbain et à vous abreuver d’un Ayran musical. 
Les événements du festival se dérouleront au <b>Maga </b> , lieu associatif situé dans le quartier très populaire de <b>Saint-Gilles </b> mais aussi 
dans le <b>snack Atlas </b> , dans le <b>bar le Waterloo </b> et dans la ville de Bruxelles, afin de proposer une forme d’art social et utopiste engageant 
un dialogue avec la population présente.</p>


  </div>
  <div class="col2">
    <h2><span>P</span> <span>r</span> <span>o</span> <span>g</span> <span>r</span> <span>a</span> <span>m</span> <span>m</span> <span>e</span></h2>
    <br>

    <h3>Vendredi 21 avril</h3>
    <ul>
      <li>
        <p class="dateType">
          <span class="date">18:00</span>
          <span class="type">O u v e r t u r e&nbsp;&nbsp;&nbsp;d e&nbsp;&nbsp;&nbsp;l ' e x p o s i t i o n</span>
            </p>
            <span class="type">S c u l p t u r e</span>
              <span class="people">
                Stephane Blumenschein</span>
              <p class="title"> Passerby (stages) </p>
              <span class="type">P e r f o r m a n c e&nbsp;&nbsp; c u l i n a i r e</span>
                 <span class="people">
                  Xavier  Gorgol  &amp;  Rareş  Crăiuţ
                    </span>
                    <p class="title"> Transition</p>
                     <span class="type">I n s t a l l a t i o n</span>
                     <span class="people">Jot Fau </span>
                       <p class="title"> Sleepy time has ended </p>
       
        <p class="place">
          Le Maga
        </p>
      </li>
      <li>
          <p class="dateType">
          <span class="date">18:30</span>
            <span class="type">p e r f o r m a n c e</span>
            </p>
            <p class="people">
              Irina Favero-Longo &amp; Rebecca Konforti
            </p>
            <p class="title">
          Posé entre le coin et la marche
        </p>
        <p class="place">
          Le Maga
        </p>
      </li>
       <li>
        <p class="dateType">
          <span class="date">19:00</span>
           <span class="type">P e r f o r m a n c e&nbsp;&nbsp;&nbsp;a v e c&nbsp;&nbsp;&nbsp;u n e&nbsp;&nbsp;&nbsp;g u i t a r e&nbsp;&nbsp;&nbsp;é l e c t r i q u e</span>
        </p>
        <p class="people">
          Otto Bystöm 
        </p>
        <p class="title">
          Slump 
        </p>
        <p class="place">
          Bar Waterloo
        </p>
      </li>
      <li>
        <p class="dateType">
          <span class="date">20:00</span>
          <span class="type">c h a n t&nbsp;&nbsp;d a n s e&nbsp;&nbsp;a y r a n</span>
        </p>
        <p class="people">
          André D.Chapatte
        </p>
        <p class="title">
          Private Utopia
        </p>
        <p class="place">
          Le Maga &amp; Snack Atlas
        </p>
      </li>
    </ul>
    <br>

    <h3>Samedi 22 avril</h3>
    <ul>
     <li>
        <p class="dateType">
          <span class="date">19:00</span>
          <span class="type">r é c i t&nbsp;&nbsp;&nbsp;m u s i c a l</span>
        </p>
        <p class="people">
          Sandra Naji &amp; Antoine Loyer
        </p>
        <p class="title">
          Rituel des vieilles de Bredene-Aan-Zee
        </p>
        <p class="place">
          Le Maga
        </p>
      </li>
         <li>
        <p class="dateType">
          <span class="date">20:00</span>
          <span class="type">P e r f o r m a n c e&nbsp;&nbsp; c u l i n a i r e</span>
        </p>
        <p class="people">
          Rareş Crăiuţ &amp; Xavier Gorgol
        </p>
        <p class="title">
        </p>
        <p class="place">
          Le Maga
        </p>
      </li>
        <li>
        <p class="dateType">
          <span class="date">21:00</span>
          <span class="type">c h a n t  d a n s e  a y r a n</span>
        </p>
        <p class="people">
          André D.Chapatte
        </p>
        <p class="title">
          Private Utopia
        </p>
        <p class="place">
          Le Maga &amp; Snack Atlas
        </p>
      </li>
      <li>
        <p class="dateType">
          <span class="date">22:00</span>
          <span class="type">P e r f o r m a n c e&nbsp;&nbsp;&nbsp;a v e c&nbsp;&nbsp;&nbsp;u n e&nbsp;&nbsp;&nbsp;g u i t a r e&nbsp;&nbsp;&nbsp;é l e c t r i q u e</span>
        </p>
        <p class="people">
          Otto Byström
        </p>
        <p class="title">Slump
        </p>
        <p class="place">
          Bar Waterloo
        </p>
      </li>
      <li>
         <p class="dateType">
          <span class="date">22:30</span>
          <span class="type">c o n c e r t&nbsp; r o c k&nbsp; p o s t&nbsp; m o d e r n e</span>
        </p>
        <p class="people">
          André D.Chapatte
        </p>
        <p class="title">
          The Zachary Airhorn Show
        </p>
        <p class="place">
          Bar Waterloo
        </p>
      </li>
        <p class="dateType">
          <span class="date">23:00</span>
          <span class="type">c o n c e r t&nbsp; T e c h n o&nbsp; e x p é r i m e n t a l e</span>
        </p>
        <p class="people">
          Maxime Lacôme
        </p>
        <p class="title">
          Minor mishap
        </p>
        <p class="place">
          Bar Waterloo
        </p>
      </li>
    </ul>
  </div>

  <div class="col3">
    <ul>

<li>
        <p class="dateType">
          <span class="date">00:00</span>
          <span class="type">M i x&nbsp;B r e a k c o r e</span>
        </p>
        <p class="people">
          Brique Corps
        </p>
        <p class="title">
        </p>
        <p class="place">
          Bar Waterloo
        </p>
      </li>
      <li>
        <p class="dateType">
          <span class="date">01:30</span>
          <span class="type">c h r o n i q u e&nbsp;&nbsp;&nbsp;m u s i c a l e&nbsp;&nbsp;&nbsp;f é m i n i s m e</span>
        </p>
        <p class="people">
          Bettina Kordjani
        </p>
        <p class="title">
          Pussies grab back
        </p>
        <p class="place">
          Bar Waterloo
        </p>
      </li>
      <p class="title">
          + des surprises pour danser toute la nuit.
        </p>
    </ul>
    <br>

    <h3>Dimanche 23 avril</h3>
    <ul>
      <li>
        <p class="dateType">
          <span class="date">15:00&thinsp;→&thinsp;18:00</span>
          <span class="type">b a l a d e</span>
        </p>
        <p class="people">
          Marine Kaiser &amp; Stéphanie Verin
        </p>
        <p class="title">
          La Ballade de Kaiser et Qasimov
        </p>
        <p class="place">
          Départ → Centre Culturel de Uccle
        </p>
      </li>
      <li>
        <p class="dateType">
          <span class="date">00:00&thinsp;→&thinsp;1:30</span>
            <span class="type">b a l a d e</span>
        </p>
        <p class="people">
          Clément Thiry
        </p>
        <p class="title">
          Visite des coins sombres de Bruxelles
        </p>
        <p class="place">
          rdv par mail&thinsp;: zderouzze@gmail.com
        </p>
      </li>
    </ul>
    <br>

    <h3>Lundi 24 avril</h3>
    <ul>
       <li>
        <p class="dateType">
          <span class="date">00:00&thinsp;→&thinsp;1:30</span>
            <span class="type">b a l a d e</span>
        </p>
        <p class="people">
          Clément Thiry
        </p>
        <p class="title">
          Visite des coins sombres de Bruxelles
        </p>
        <p class="place">
          rdv par mail&thinsp;: zderouzze@gmail.com
        </p>
      </li>
    </ul>
    <br>

    <h3>mardi 25 avril</h3>
    <ul>
       <li>
        <p class="dateType">
          <span class="date">00:00&thinsp;→&thinsp;1:30</span>
            <span class="type">b a l a d e</span>
        </p>
        <p class="people">
          Clément Thiry
        </p>
        <p class="title">
          Visite des coins sombres de Bruxelles
        </p>
        <p class="place">
          rdv par mail&thinsp;: zderouzze@gmail.com
        </p>
      </li>
    </ul>
    <br>

    <h3>Mercredi 26 avril</h3>
    <ul>
      <li>
        <p class="dateType">
          <span class="date">19h30</span>
          <span class="type">B a l a d e &nbsp;&nbsp;d e&nbsp;&nbsp; b a r&nbsp;&nbsp; e n&nbsp;&nbsp; b a r&nbsp;&nbsp; e t&nbsp;&nbsp; l e c t u r e s</span>
        </p>
        <p class="people">
          POP (Potential Office Project)
        </p>
        <p class="title">
        </p>
        <p class="place">
          Départ → Le Maga - arrivée → Le Midpoint Café
        </p>
      </li>
       <li>
        <p class="dateType">
          <span class="date">00:00&thinsp;→&thinsp;1:30</span>
            <span class="type">b a l a d e</span>
        </p>
        <p class="people">
          Clément Thiry
        </p>
        <p class="title">
          Visite des coins sombres de Bruxelles
        </p>
        <p class="place">
          Départ → Le Midpoint Café
        </p>
      </li>
    </ul>
  </div>

  <div class="col4">
    <h3>Jeudi 27 avril</h3>
    <ul>
      <li> 
        <p class="dateType">
          <span class="date">18:00 </span>
           <span class="type">P e r f o r m a n c e&nbsp;&nbsp; c u l i n a i r e</span>
             </p>
             <p class="people">
              Stéphanie Becquet, Marjolein Guldentops et Romy Vanderveken 
            </p>
        <p class="title"> 
          Kitchen Session
        </p> 
        <p class="place">
          Chaussée de Forest, 106, Saint-Gilles</p>
    </ul>
    <br>

    <h3>Vendredi 28 avril</h3>
    <ul>
      <li>
         <p class="dateType">
        <span class="date">10:00 → 13:00 </span> 
        <span class="type">b a l a d e</span>
         </p>
             <p class="people">
        Marine Kaiser et Stéphanie Verin 
         </p>
        <p class="title">
          La ballade de Kaiser et Qasimov
        </p> 
        <p class="place">
           Départ → Centre Culturel de Uccle</li>
      </p>
    </ul>
    <br>

<ul>
      <li>
         <p class="dateType">
        <span class="date">18:00 </span> 
        <span class="type">P e r f o r m a n c e&nbsp;&nbsp; c u l i n a i r e</span>
     <p class="title"> 
          Kitchen Session
        </p> 
         <p class="people">
              Stéphanie Becquet, Marjolein Guldentops et Romy Vanderveken 
            </p>
        <p class="place">
          Chaussée de Forest, 106, Saint-Gilles</p>
    </ul>
    <br>

    <h3>Dimanche 30 avril</h3>
    <ul>
 <p class="dateType">
        <span class="date">18:00</span>   
        <span class="type">L a n c e m e n t&nbsp;&nbsp; d u&nbsp;&nbsp; l i v r e</span>   
        <p class="title">livre Poisson-évêque</p> 
        <p class="people"> Romain Marula, Étienne Ozeray et Stéphanie Verin</p>
      <p class="place">Le Maga</p>
      </li>

        <li>
          <p class="dateType">
          <span class="date">19:00</span>
            <span class="type">p e r f o r m a n c e</span>
            </p>
            <p class="people">
              Irina Favero-Longo &amp; Rebecca Konforti
            </p>
            <p class="title">
          Posé entre le coin et la marche
        </p>
        <p class="place">
          Le Maga
        </p>
      </li>
  
          

    </ul>
    <br>
    <br>

    <div class="crédits">
      <p>
        Un festival organisé par Stéphanie Verin, asssistée d'Émilie Salson
      </p>
      <p>
        Design graphique: Romain Marula &amp; Étienne Ozeray
      </p>
    </div>
  </div>
</div>

<div id="poisson">

  <div class="col1">
    <h1>Festival Poisson-Évêque</h1>
    <h2>du 21 au 30 avril 2017</h2>
    <h2>Le Maga, avenue Jean Volders, 56, 1060 S<sup>t</sup>-Gilles</h2>
    <h2>Bar Waterloo, Chaussée de Waterloo, 217, 1060 S<sup>t</sup>-Gilles</h2>
    <h2>Snack Atlas, Parvis de la Trinité, 6, 1060 S<sup>t</sup>-Gilles</h2>
    <br>
    <p>
      Le festival Poisson-Evêque est une première édition regroupant des artistes de Finlande, de Suisse, de France, de Hollande, de Belgique et de Roumanie ayant une pratique de la sculpture, de la performance et de la ballade. Les actions proposés questionneront la gentrification, les féminismes émergeants, les effets sociaux du capitalisme et les manières de se réapproprier les outils techniques et technologiques contemporains. Les événements du festival se dérouleront au Maga, lieu associatif situé dans le quartier très populaire de Saint-Gilles mais aussi dans des snacks, des bars et dans la ville de Bruxelles, afin de proposer une forme d'art social et utopiste engageant un dialogue avec la population présente.
    </p>
    <br>
    <br>

  </div>
  <div class="col2">
    <h2>Programme</h2>
    <br>

    <h3>Vendredi 21 avril</h3>
    <ul>
      <li>18:00 > Jot Fau, Stephane Blumenschein, Xavier Gorgol et Rareş Crăiuţ, Irina Favero-Longo > Vernissage de l'exposition > Maga</li>
      <li>19h30 > Irina Favero-Longo et Rébecca Konforti > Posé entre le coin et la marche > performance > Maga > 30 mn</li>
      <li>20:00 > André D. Chapatte > performance > Maga et Snack Atlas > 40 mn</li>
    </ul>
    <br>

    <h3>Samedi 22 avril</h3>
    <ul>
      <li>19:00 > Rareş Crăiuţ et Xavier Gorgol > Transition > performance / conversation publique > Maga > 40 mn</li>
      <li>20:00 > Sandra Naji et Antoine Loyer > Rituel des vieilles de Bredene-Aan-Zee> récit musical > Maga > 20mn</li>
      <li>21:00 > Otto Byström > Performance avec une guitare électrique > Bar Waterloo > 20 mn</li>
      <li>21h30 > André D. Chapatte > Zachary Airhorn Show > concert > Bar Waterloo > 40mn</li>
      <li>22h30 > Maxime Lacôme > Concert > Bar Waterloo > 40 mn</li>
    </ul>
  </div>

  <div class="col3">
    <ul>
      <li>23h30 > Brique Corps > Mix de Breakcore > Bar Waterloo > 40 mn</li>
      <li>00h30 > Bettina Kordjani > Pussies grab back > chronique musicale du féminisme > Bar Waterloo > 1h30</li>
    </ul>
    <br>

    <h3>Dimanche 23 avril</h3>
    <ul>
      <li>15h-18h > Marine Kaiser et Stéphanie Verin > La ballade de Kaiser et Qasimov (épisode 1) > balade > Rendez-vous entrée Centre culturel de Uccle</li>
      <li>00:00- 1h30 > Clément Thiry > Visite des coins sombres de Bruxelles > lieu de rdv à déterminer</li>
    </ul>
    <br>

    <h3>Lundi 24 avril</h3>
    <ul>
      <li>00h-1h30 > Clément Thiry > Visite des coins sombres de Bruxelles > lieu de rdv à déterminer</li>
    </ul>
    <br>

    <h3>mardi 25 avril</h3>
    <ul>
      <li>00:00-1h30 > Clément Thiry > Visite des coins sombres de Bruxelles > lieu de rdv à déterminer</li>
    </ul>
    <br>

    <h3>Mercredi 26 avril</h3>
    <ul>
      <li>19h30 > POP (Potential Office Project) > Balade de bar en bar et lectures > Départ > Maga > Point d’arrivée > Le Midpoint Café</li>
      <li>00:00- 1h30 > Clément Thiry > Visite des éclairages défectueux de Bruxelles > départ Midpoint Café</li>
    </ul>
  </div>

  <div class="col4">
    <h3>Jeudi 27 avril</h3>
    <ul>
      <li>18:00 > Stéphanie Becquet, Marjolein Guldentops et Romy Vanderveken > Kitchen Session > Chaussée de Forest, 106, Saint-Gilles</li>
    </ul>
    <br>

    <h3>Vendredi 28 avril</h3>
    <ul>
      <li>10h-13h > Marine Kaiser et Stéphanie Verin > La ballade de Kaiser et Qasimov (épisode 2) > Départ > Jardin du musée Royal de Belgique.</li>
      <li>18:00 > Stéphanie Becquet, Marjolein Guldentops et Romy Vanderveken > Kitchen Session > Chaussée de Forest, 1060, Saint-Gilles</li>
    </ul>
    <br>

    <h3>Dimanche 30 avril</h3>
    <ul>
      <li>Lancement de l’édition du festival Poisson-Evêque > Maga</li>
      <li>19:00 > Irina Favero Longo et Rebecca Konforti > Posé entre le coin et la marche > performance > Maga > 30 mn</li>
    </ul>
    <br>
    <br>

    <div class="crédits">
      <p>
        Un festival organisé par Stéphanie Verin, asssistée d'Émilie Salson
      </p>
      <p>
        Design graphique: Romain Marula &amp; Étienne Ozeray
      </p>
    </div>
  </div>
</div>
