

<div class="contentcontent">

	<div class="titre"> <span><b>Festival</b>&thinsp; &thinsp;Poisson-Évêque</span>  </div><div class="date">21&thinsp;<b>avril&thinsp;—</b>&thinsp;30&thinsp;<b>avril&thinsp;2017</b></div> 

<!--
	<div class="date">

	</div> -->


	<div class="people">
		<!-- <p class="curatrice"><b>Curatrice&thinsp;:</b>
			Un festival organisé par Stéphanie Vérin.
		</p> -->
		<ul><li class="list1"><span>Stéphanie</span> <sup>P e r f o r m a n c e  c u l i n a i r e</sup> <span>Becquet</span></li>
			<li class="list2"><span>Stephan</span> <sup>S c u l p t u r e</sup> <span>Blumenschein</span></li>
			<li class="list3"><span>Otto</span> <sup>P e r f o r m a n c e</sup> <span>Byström</span></li>
			<li class="list4"><span>André</span> <sup>C o n c e r t</sup> <span>D.Chapatte</span></li>
			<li class="list5"><span>Brique</span> <sup>C o n c e r t</sup> <span>Corps</span></li>
			<li class="list6"><span>Rareş</span> <sup>P e r f o r m a n c e  c u l i n a i r e</sup> <span>Crăiuţ</span></li>
			<li class="list7"><span>Jot</span> <sup>I n s t a l l a t i o n</sup> <span>Fau</span></li>
			<li class="list8"><span>Irina</span> <sup>P e r f o r m a n c e</sup> <span>Favero-Longo</span></li>
			<li class="list9"><span>Xavier</span>  <sup>P e r f o r m a n c e  c u l i n a i r e</sup> <span>Gorgol</span></li>
			<li class="list10"><span>Marjolein</span> <sup>P e r f o r m a n c e  c u l i n a i r e</sup> <span>Guldentops</span></li>
			<li class="list11"><span>Marine</span> <sup>R e p o r t a g e   s i t u é</sup> <span>Kaiser</span></li>
			<li class="list12"><span>Rebecca</span> <sup>P e r f o r m a n c e</sup> <span>Konforti</span></li>
			<li class="list13"><span>Bettina</span> <sup>C h r o n i q u e  m u s i c a l e</sup> <span>Kordjani</span></li>
			<li class="list14"><span>Maxime</span> <sup>C o n c e r t</sup> <span>Lacôme</span></li>
			<li class="list15"><span>Antoine</span> <sup>R é c i t  m u s i c a l</sup> <span>Loyer</span></li>
			<li class="list16"><span>Sandra</span> <sup>R é c i t  m u s i c a l</sup> <span>Naji</span></li>
			<li class="list17" style="text-align:left;"><span>Potential <sup>B a l a d e - L e c t u r e</sup>  Office&thinsp;Project</span></span></li>
			<li class="list18"><span>Clément</span> <sup>B a l a d e</sup> <span>Thiry</span></li>
			<li class="list19"><span>Romy</span> <sup>P e r f o r m a n c e  c u l i n a i r e </sup> <span>Vanderveken</span></li>	
			<li class="list20"><span>Stéphanie</span> <sup>R e p o r t a g e   s i t u é</sup> <span>Verin</span></li>
		</ul>
	</div>


	<p class="lieu"><span>Le Maga<b> Avenue Jean Volders,</b> 56</span></br>
	<span>Bar Waterloo<b> Chaussée de Waterloo, </b>217</span></br>
	<span>Snack Atlas<b> Parvis de la Trinité,</b>6</span></br>
	<span><b>Saint&thinsp;-&thinsp;Gilles,&thinsp;</b>1160</span></p>

<!--	<?php include('texte-steph.php') ?>-->

</div>

<div class="marge margeHoriz margeTop"></div>
<div class="marge margeHoriz margeBottom"></div>
<div class="marge margeVert margeLeft"></div>
<div class="marge margeVert margeRight"></div>

<!-- <div id="gridVisible">
	<div class="rows">
		<div class="line horizontal row"></div>
	</div>
	<div class="cols">
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
	</div>
</div> -->



<div class="rectangle"></div>
