

<div class="contentcontent">

	<div class="titre"> <span><b>Festival</b> Poisson-Évêque</span>  </div><div class="date">21<b>avril</b> — 30<b>avril2017</b> </div>

<!--
	<div class="date">

	</div> -->


	<div class="people">
		<!-- <p class="curatrice"><b>Curatrice&thinsp;:</b>
			Un festival organisé par Stéphanie Vérin.
		</p> -->
		<ul><li class="list1"><span>Stéphanie</span> <sup><span>P</span> <span>e</span> <span>r</span> <span>f</span> <span>o</span> <span>r</span> <span>m</span> <span>a</span> <span>n</span> <span>c</span> <span>e</span>&nbsp;&nbsp;<span>c</span> <span>u</span> <span>l</span> <span>i</span> <span>n</span> <span>a</span> <span>i</span> <span>r</span> <span>e</span></sup> <span>Becquet</span></li>
			<li class="list2"><span>Stephan</span> <sup><span>S</span> <span>c</span> <span>u</span> <span>l</span> <span>p</span> <span>t</span> <span>u</span> <span>r</span> <span>e</span></sup> <span>Blumenschein</span></li>
			<li class="list3"><span>Otto</span> <sup><span>P</span> <span>e</span> <span>r</span> <span>f</span> <span>o</span> <span>r</span> <span>m</span> <span>a</span> <span>n</span> <span>c</span> <span>e</span></sup> <span>Byström</span></li>
			<li class="list4"><span>André</span> <sup><span>C</span> <span>o</span> <span>n</span> <span>c</span> <span>e</span> <span>r</span> <span>t</span></sup> <span>D.Chapatte</span></li>
			<li class="list5"><span>Brique</span> <sup><span>C</span> <span>o</span> <span>n</span> <span>c</span> <span>e</span> <span>r</span> <span>t</span></sup> <span>Corps</span></li>
			<li class="list6"><span>Rareş</span> <sup><span>P</span> <span>e</span> <span>r</span> <span>f</span> <span>o</span> <span>r</span> <span>m</span> <span>a</span> <span>n</span> <span>c</span> <span>e</span>&nbsp;&nbsp;<span>c</span> <span>u</span> <span>l</span> <span>i</span> <span>n</span> <span>a</span> <span>i</span> <span>r</span> <span>e</span></sup> <span>Crăiuţ</span></li>
			<li class="list7"><span>Jot</span> <sup><span>I</span> <span>n</span> <span>s</span> <span>t</span> <span>a</span> <span>l</span> <span>l</span> <span>a</span> <span>t</span> <span>i</span> <span>o</span> <span>n</span></sup> <span>Fau</span></li>
			<li class="list8"><span>Irina</span> <sup><span>P</span> <span>e</span> <span>r</span> <span>f</span> <span>o</span> <span>r</span> <span>m</span> <span>a</span> <span>n</span> <span>c</span> <span>e</span></sup> <span>Favero-Longo</span></li>
			<li class="list9"><span>Xavier</span>  <sup><span>P</span> <span>e</span> <span>r</span> <span>f</span> <span>o</span> <span>r</span> <span>m</span> <span>a</span> <span>n</span> <span>c</span> <span>e</span>&nbsp;&nbsp;<span>c</span> <span>u</span> <span>l</span> <span>i</span> <span>n</span> <span>a</span> <span>i</span> <span>r</span> <span>e</span></sup> <span>Gorgol</span></li>
			<li class="list10"><span>Marjolein</span> <sup><span>P</span> <span>e</span> <span>r</span> <span>f</span> <span>o</span> <span>r</span> <span>m</span> <span>a</span> <span>n</span> <span>c</span> <span>e</span>&nbsp;&nbsp;<span>c</span> <span>u</span> <span>l</span> <span>i</span> <span>n</span> <span>a</span> <span>i</span> <span>r</span> <span>e</span></sup> <span>Guldentops</span></li>
			<li class="list11"><span>Marine</span> <sup><span>R</span> <span>e</span> <span>p</span> <span>o</span> <span>r</span> <span>t</span> <span>a</span> <span>g</span> <span>e</span>&nbsp;&nbsp;<span>s</span> <span>i</span> <span>t</span> <span>u</span> <span>é</span></sup> <span>Kaiser</span></li>
			<li class="list12"><span>Rebecca</span> <sup><span>P</span> <span>e</span> <span>r</span> <span>f</span> <span>o</span> <span>r</span> <span>m</span> <span>a</span> <span>n</span> <span>c</span> <span>e</span></sup> <span>Konforti</span></li>
			<li class="list13"><span>Bettina</span> <sup><span>C</span> <span>h</span> <span>r</span> <span>o</span> <span>n</span> <span>i</span> <span>q</span> <span>u</span> <span>e</span>&nbsp;&nbsp;<span>m</span> <span>u</span> <span>s</span> <span>i</span> <span>c</span> <span>a</span> <span>l</span> <span>e</span></sup> <span>Kordjani</span></li>
			<li class="list14"><span>Maxime</span> <sup><span>C</span> <span>o</span> <span>n</span> <span>c</span> <span>e</span> <span>r</span> <span>t</span></sup> <span>Lacôme</span></li>
			<li class="list15"><span>Antoine</span> <sup><span>R</span> <span>é</span> <span>c</span> <span>i</span> <span>t</span>&nbsp;&nbsp;<span>m</span> <span>u</span> <span>s</span> <span>i</span> <span>c</span> <span>a</span> <span>l</span></sup> <span>Loyer</span></li>
			<li class="list16"><span>Sandra</span> <sup><span>R</span> <span>é</span> <span>c</span> <span>i</span> <span>t</span>&nbsp;&nbsp;<span>m</span> <span>u</span> <span>s</span> <span>i</span> <span>c</span> <span>a</span> <span>l</span></sup> <span>Naji</span></li>
			<li class="list17" style="text-align:left;"><span>Potential&thinsp;Office <sup><span>B</span> <span>a</span> <span>l</span> <span>a</span> <span>d</span> <span>e</span> <span>-</span> <span>L</span> <span>e</span> <span>c</span> <span>t</span> <span>u</span> <span>r</span> <span>e</span></sup>  Project</span></span></li>
			<li class="list18"><span>Clément</span> <sup><span>B</span> <span>a</span> <span>l</span> <span>a</span> <span>d</span> <span>e</span></sup> <span>Thiry</span></li>
			<li class="list19"><span>Romy</span> <sup><span>P</span> <span>e</span> <span>r</span> <span>f</span> <span>o</span> <span>r</span> <span>m</span> <span>a</span> <span>n</span> <span>c</span> <span>e</span>&nbsp;&nbsp;<span>c</span> <span>u</span> <span>l</span> <span>i</span> <span>n</span> <span>a</span> <span>i</span> <span>r</span> <span>e</span></sup> <span>Vanderveken</span></li>
			<li class="list20"><span>Stéphanie</span> <sup><span>R</span> <span>e</span> <span>p</span> <span>o</span> <span>r</span> <span>t</span> <span>a</span> <span>g</span> <span>e</span>&nbsp;&nbsp;<span>s</span> <span>i</span> <span>t</span> <span>u</span> <span>é</span></sup>  <span>Verin</span></li>
		</ul>
	</div>


	<p class="lieu"><span><b>Le Maga</b>, Avenue Jean Volders 56</span></br>
	<span><b>Bar Waterloo</b>, Chaussée de Waterloo 217</span></br>
	<span> <b>Snack Atlas</b>, Parvis de la Trinité 6</span></br>
	<span>1160 <b>Saint-Gilles</b></span></p>

<!--	<?php include('texte-steph.php') ?>-->

</div>

<div class="marge margeHoriz margeTop"></div>
<div class="marge margeHoriz margeBottom"></div>
<div class="marge margeVert margeLeft"></div>
<div class="marge margeVert margeRight"></div>

<!-- <div id="gridVisible">
	<div class="rows">
		<div class="line horizontal row"></div>
	</div>
	<div class="cols">
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
	</div>
</div> -->

<!-- <img class="bgOr" src="img/Hobos2.jpg" alt="" /> -->

<div class="rectangle"></div>
