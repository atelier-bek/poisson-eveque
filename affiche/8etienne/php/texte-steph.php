<div class="texteSteph">
  Ballade avec Marine Kaiser, 12/01/17, Commune de Saint-Gilles, Chaussée
  de Waterloo, Ecole ? , AMO Place Morichard, Parc Paulus (aire de jeu),
  Pizzeri Trattoria ai 6 angoli, Pizza Ortolana, légumes de saison, eau plate,
  café, liqueur de café anisée, propriétaire Pier-Giorgio. 10h30-14h.
  Je croise José sur le retour, Rue de Bordeaux. Il vient du Portugal et il vend
  ce qu'il chine dans son garage. digne d'une affiche de théâtre ou de musée, joliement anachronique.
  Marine remarque une table qui donne sur le parvis, elle s'est toujours
  dit qu'on pourrait y faire des réunions. Nous demandons l'horaire de la
  commune. C'est un peu décevant, ouvert tous les matins jusque 12h et
  le mardi uniquement jusque 19h. On pourra y prendre nos petits déjeu-
  ners.
  "-Boooooooonjour !
  -Booooooooooonjour Marine !
  -Comment ça va ?
  -Bien et toi ?
  -Bien! Bien ! Tu as vu comme cette sculpture est incroyable ? C'est pour
  ça que je voulais te donner rdv ici. Regarde c'est incroyable ces buissons
  autour, c'est absurde !
  -Oh oui, cette femme nue a des fesses incroyables et de gros seins, elle a l'air
  incroyablement joyeuse dans cette position !
  -Oui ! Et il m'est impossible de savoir qui l'a faite !
  -Ah oui ? Elle n'est pas signée ? Marine, je vais prendre en photo les points
  forts de notre ballade, les femmes à l'honneur." Chaussée de Waterloo, on remarque un appartement en contrebas à
  louer. C'est à louer depuis un an et demi. J'appelle le propriétaire:
  Marine me propose de rentrer dans la commune de Saint-Gilles que je n'ai
  jamais visitée. Nous avions pour projet de marcher jusque Uccle ou bien au
  centre jusqu'à une autre statut qui ressemble beaucoup à celle que l'on vient
  d'observer. Nous avions apprécié le travail de curation de Anna Miletic sur
  la représentation des femmes dans l'espace public. Sans vraiment le décider,
  nous en ferons, le fil conducteur de nos ballades. Le plaisir, ici, sera de se
  perdre, de perdre le fil des conversations, de rire, de découvrir des choses
  qu'on ne pourraient pas voir si on ne sortait pas de nos habitudes. Marine
  devient une compagne de voyage, le voyage peut se faire au coin de la rue, à
  5 mn de chez soi. La ballade, c'est ne pas savoir. Au cours de cette ballade,
  notre enthousiasme débordant fait qu'on reste très longtemps dans cette
  commune à y trouver des tas de loufequeries, drôleries et absurdités.
  Dans la commune de Saint Gilles, les sculptures et les peintures sont kitshs,
  assez mal positionnés dans l'espace mais surtout source de fous rires incom-
  mensurables. Non pas parce qu'elles n'ont pas de valeur à nos yeux mais
  parce que la maladresse a un charme inestimable qui ravit nos imaginations.
  Nous imaginons avec joie que l'artiste Lambeau est l'arrière grand père
  décédé de William Lambeau, peintre étudiant à l'Erg. Nous fantasmons
  beaucoup d'histoires avec Marine.
  Marine me fait observer le charme caché des plantes artificielles :
  "Allo oui, Bonjour, je vous appelle pour l'appartement, chaussée de
  Waterloo, en face du bar de la pompe.
  - Oui, je ne suis pas le propriétaire mais je peux vous transmettre toutes
  les informations dont vous avez besoin. C'est pour combien de per-
  sonnne ?
  - Deux personnes, moi et mon compagnon.
  - Vous avez des revenus fixes ?
  - Oui oui un Cdi dans l'alimentaire.
  - Et votre compagnon ?
  - un Cdi aussi, il travaille dans le service publique à la bibliothèque de
  Bruxelles.
  - Ah très bien. Vous savez c'est un immeuble classé historiquement,
  construit par Horta. Il y a aussi des appartements à l'étage.
  - Ah oui ? Je suppose qu'il sont différents de ce qu'on voit en bas avec le
  jardin ?
  - Oui, mieux équipés au niveau de la cuisine un peu plus cher.
  - Et le bas, combien c'est justement ?
  - 650 euros et 50 euros de charges pour l'eau et l'electrité.
  -Ah oui, c'est interessant.
  - Mais nous n'avons pas vraiment parlé de vos revenu, parce que ce sont
  les revenus net qui interessent le propriétaire.
  - 1200 net pour mois monsieur.
  - Et votre ami ?
  - Un peu plus, 1500 euros.
  - Ah, tout vas bien alors.
  - Et bien écoutez, merci pour toutes vos informations monsieur, bonne
  journée."
  Nous passons de la salle de mariage. le parquet au sol est usé, les sièges en
  cuirs sont impressionnant, plus confortables au devant, proche du maire,
  avec un petit dossier pour le dos, et plus on recule dans la salle, de moins
  en moins confortables. Je dis à Marine que ce qui me plait le plus dans cette
  salle, c'est ce piano quart de queue que j'imagine accueillir un pianiste fou
  qui ferait dégénérer la scène digne d'un festin grec. Des gens nus, un jeu sur
  la table du maire, un énorme gateau sur les petits poufs des mariés. Marine rit et me prend en photo quand je joue à la dame au téléphone.
  Nous continuons la ballade. On découvre une école, ancienne halle avec
  une grande verrière, magnifique. Je ris en voyans l'institutrice arriver. Je
  m'amuse à lui dire que je suis la babysitter d'un petit garçon qui étudie
  et qui s'est égarée à explorer la beauté des lieux augmenté par le charme
  des plantes vertes.
  Mon mensonge est tout petit, mignon et peu crédible. On sort gentie-
  ment pour respecter la tranquilité des lieux.
  Je dis à Marine. Bon j'arrête avec mes impertinents mensonges. Ayons
  de vrais conversations avec les gens à la prochaine rencontre.
  Marine était fascinée par la petitesse des meubles fait à la taille des
  enfants. Une école, un monde construit à leur echelle.
  On passe devant l'AMO, on y rencontre un animateur culturel charmant
  qui nous explique comment fonctionne la salle d'escalade.
  Au détour d'un panneau qui nous explique que le moineau est en dispa-
  rition à Bruxelles et que la ville a planté des fleurs sauvages sur les par-
  terres et qu'il seraient bon de les respecter, nous continuons notre route.
  Au premier étage nous remarquons les détails d'un couloir donnant sur les
  bureaux des employés de la commune. Une affiche de proposition de Bourse "- Tu connais le parc Paulus ?
  - Non.
  - L'avantage des plantes en plastiques c'est qu'ils peuvent marier des plantes
  qui n'ont plus les mêmes fonctions. ici il y a un palmier qui a besoin de
  lumière, ici une fougère, une ombéllifère, et une plante vivace commune en
  dessous. C'est une chose qui ne serait pas possible dans la nature. Et alors ils
  reproduisent ces compositions partout dans la commune regarde !
  -Alors, allons-y, tu sais que j'avais participé à cette appel du Kaii à
  reflechir autour de la temporalité et des saisons. J'ai rencontré la res-
  ponsable des espaces verts de Saint gilles qui me racontait que son parc
  préféré était celui là parce que c'est ici qu'elle a le plus de liberté. Elle
  m'a raconté qu'elle essayait de ne pas mettre d'engrais et de ne pas trop
  tailler et modifier la flore.
  (Je regarde autour de moi et les buis sont taillés, la pelouse tondue,
  j'imagine que les normes d'entretien de la ville ne sont pas si faciles à
  détourner).
  -Elle a du couper un arbre dans à l'entrée de ce parc et elle s'est dit
  qu'elle pouvait alors en faire une table avec la souche restantes et des
  bancs avec le tronc. Je remarque qu'ils sont utilisés.
  - La convivialité est partout. Mais qui était ce Paulus ?
  - Je sais seulement qu'il vivait dans cette grande maison qui donne sur
  le parc, c'était le propriétaire des lieux.
  -Et c'est devenu public très certainement à sa mort ?
  -Peut-être ! "
  La convivialité commence avec une table. Si nous sommes dans la
  ville et que nous discutons, la ville devient conviviale mais comment
  posons nous la convivialité entre nous ? La convivialité commence avec
  une table et elle encore meilleure quand la table est devant nous, nous
  regardons dans la même direction, épaule contre épaule.
  Nous jouons dans le petit parc à jeux. Marine imagine une réunion à
  13 ou 14 participants sur une structure en étoile. Je lui dis que c'est
  une structure idéale parce qu'on peut permutter entre la périphérie et
  le centre, comme un oratoire flexible, circulaire et amusant. Sans chef,
  sans hiérarchie, à tour de rôle chacun pourrait y déclamer un texte, une
  idée, une reflexion, y chanter une chanson. Je pense à mon ami Antoine
  qui dit toujours : " Avec une chanson, tu arrives quelque part et paf!
  tu donnes quelque chose aux gens, avec rien, juste avec toi et ta voix,
  tu peux être chez toi partout et les gens si ils le recoivent sont remplis
  d'amour." La définition de la convivialité pour lui commence par une
  chanson.
  J'ai l'idée d'axer la prochaine ballade à Uccle vers un parc que la com-
  mune veut raser pour y contruire des logements sociaux. Les habitants
  de la commune résistent depuis plusieurs mois à sa destruction.
  Avec Marine, nous éprouvons un plaisir fou à décrypter les intentions
  cachées que l'on trouve derrière les œuvres omniprésentes et quasi invi-
  sibles de nos espaces publiques.

  Discussion avec André D. Chapatte sur Messenger, 28/11/2016
  Charlie Qasimov : J’essaye de rendre une petite présentation du projet
  pour le 1er décembre, dans deux jours. Ce serait fantastique si tu expliques
  juste en un petit paragraphe ton idée de performance dans un snack !
  André.D. Chapatte : « André Chapatte propose une installation du « Za-
  chary Airhorn Show », un spectacle solo de danse et de musique, dans un
  lieu moins communément utilisé comme une scène : un snack- bar typique
  de Bruxelles. Avec des moyens modestes, André Chapatte crée un monde
  parallèle où les situations compliquées et ambiguëes de la vie sont traitées
  avec énergie et humour. Mélangeant les références de cultures populaires,
  « The Zachary Airhorn Show » promet une explosion colorée au sein des
  lieux choisis allant de galeries d’art aux salons populaires. Un mix de high
  art and low art qui va régaler le publique ! »
  C.Q : Quelles solutions tu trouverais pour faire le lien entre le snack choisi
  et le maga ? ( Comment emmener les spectateurs ? Par rdv, etc... )
  A.D.C : J’ai trouvé sur internet une sorte de petite box avec laquelle tu peux
  faire le « guide touristique ». C'est une sorte de petit speaker connecté à
  un micro au visage qui peut aussi jouer de la musique. Tout ça se met sur
  la ceinture. Je crois que ça offre un peu plus de flexibilité pour les proprié-
  taires. Donc :
  " Avec l’aide d’un dispositif audio conçu pour les guides touristiques, André
  Chapatte rassemblera le publique intéressé au Maga (vraisemblablement
  à une heure annoncée) et les emmènera au Snack-bar partenaire afin d’y
  présenter un élément de son spectacle. Selon la disponibilité des snacks
  partenaires, il se peut que le spectacle se déroule dans plusieurs lieux."
  C.Q : Tu as le don d'ubiquité ?
  A.D.C : :)))
  17/12/16- 13h- Repas chez moi au 73A rue Pierre Decoster. Potirons grillés
  au four et son dhal de lentille coco épicé à l'indienne, thé, chocolat marzi-
  pan amaretto.
  André D. Chapatte : Je ne sais pas si je peux parler de mon travail comme
  ça. Résumer son travail c'est toujours quelque chose de très difficile. Je
  crois que j'ai envie de laisser les gens faire leur propre interprétation du
  contenu de ce que je propose.
  Charlie Qasimov : Je comprends. C'est pour ça que je propose une forme de
  dialogue ouvert sur le travail, une forme d'entretien permanent. Je pense
  que par le dialogue, il y a beaucoup de compréhension qui peut émerger à
  la fois pour celui qui crée et celui qui reçoit.
  Qu'est ce que tu as prévu pendant ton spectacle, tu danses, tu chantes ou
  tu parles ?
  A.D.C: Oui il y a de ça. Je veux surtout tenter de créer un lien entre le public
  et ce que je propose. Dans le dernier spectacle, j'en suis venu assez naturel-
  lement à parler du rejet. Qui rejette qui ? De rejeter quelqu'un par amour.
  De rejeter une certaine forme de violence. Je propose quelque chose de très
  bizarre. Je porte un wonderbra rose de sport à l'envers, ce qui souligne les
  pectoraux et par dessus un cuir en passant moi-même par des états assez
  archetypé ce que signifie devoir être un homme fort.
  C.Q : Quels sont les outils ou les accessoires que tu utilises sur scène pour
  appuyer ton récit ?
  A.D.C : J'ai un microphone, des cigarettes avec une goutte de sang dessus
  que je distribue au public, la veste en cuir, mon smartphone, une couteau
  à cran, le wonderbra. J'utilise le microphone ou je ne l'utilise pas selon les
  moments. Pour l'instant je n'ai pas ajouté de décor comme ce que je faisais
  précedemment.
  C.Q: Ca me fait penser à ce film américain que je regardais quand j'étais
  mome, " My girl ". A un moment, les deux gosses, amis et amoureux, font un
  pacte de sang en se faisant une entaille dans la main afin de mélanger leur
  sang et de s'appartenie l'un et l'autre à tout jamais. Ici tu donnes un peu de
  ton sang au public.
  A.D.C: Je peux penser à ce que veut dire ce symbole en effet.
  C.Q: Est ce que tu veux faire un spectacle de ce genre dans le snack ?
  A.D.C: Non, pas vraiment. Je veux que ce soit en français. Dans ce que je
  décris, je parle surtout en anglais avec certains moments en français.
  Au téléphone, 12/01//17
  A.D.C : Tu saurais me donner les dates de l'évènement au Maga ?
  C.Q: Oui ce sera le week end de 21/22/23 avril 2017 en temps forts, puis
  des évènements toute la semaine suivante jusqu'au dimanche 30 avril de
  manière plus éparses. Ce serait bien que tous les spectacles/shows tiennent
  le même week end et que ce soit rejoué si il faut en semaine, je ne sais pas
  encore comment je vais répartir ce que chacun fait temporellement.
  J'ai envie qu'il y ait un contraste entre des pratiques de performances assez
  fortes, assumés au niveau de la présence corporelle par des spectacles à
  travailler dans l'espace avec des formes beaucoup plus discrètes, quasi invi-
  sibles et que ces deux choses tiennent ensembles au maga.
  A.D.C : Je pense à construire quelque chose, une scène pour l'occasion, à
  l'intérieur du maga, comme un point d'accroche qui permet aux visiteurs
  d'avoir quelque chose à regarder même quand le spectacle n'est plus là. Tra-
  vailler avec cette idée de générosité, de visibilité.
  A.D.C: Dans le dernier spectacle que j'ai monté, je recherche une forme
  hybride. Je danse en chantant, je chante juste, je parle quand je chante. Je
  switch d'un moment que je répète beaucoup et qui est maitrisé où le "per-
  sonnage" parle au public, à des moments de relachements où je suis "juste
  moi". C.Q: Je suis complètement pour cette idée d'être généreux et qu'il y ait des
  contrepoints matériels dans l'espace en contraste avec des moments forts
  de présences. Si les gens se déplacent et qu'ils ont loupé un spectacle, une
  scène ouverte, un moment d'échange, il y aura quand même quelque chose à
  voir, une trace, quelque chose à percevoir et à lire.
  C.Q: Tu veux dire que tu joues entre les codes scéniques du maintien et
  ceux du relachement des coulisses pendant le spectacle ? A.D.C: Qui sera présent alors pour cet évènement ?

  Discussion avec André D. Chapatte sur Messenger, 28/11/2016
  Charlie Qasimov : J’essaye de rendre une petite présentation du projet
  pour le 1er décembre, dans deux jours. Ce serait fantastique si tu expliques
  juste en un petit paragraphe ton idée de performance dans un snack !
  André.D. Chapatte : « André Chapatte propose une installation du « Za-
  chary Airhorn Show », un spectacle solo de danse et de musique, dans un
  lieu moins communément utilisé comme une scène : un snack- bar typique
  de Bruxelles. Avec des moyens modestes, André Chapatte crée un monde
  parallèle où les situations compliquées et ambiguëes de la vie sont traitées
  avec énergie et humour. Mélangeant les références de cultures populaires,
  « The Zachary Airhorn Show » promet une explosion colorée au sein des
  lieux choisis allant de galeries d’art aux salons populaires. Un mix de high
  art and low art qui va régaler le publique ! »
  C.Q : Quelles solutions tu trouverais pour faire le lien entre le snack choisi
  et le maga ? ( Comment emmener les spectateurs ? Par rdv, etc... )
  A.D.C : J’ai trouvé sur internet une sorte de petite box avec laquelle tu peux
  faire le « guide touristique ». C'est une sorte de petit speaker connecté à
  un micro au visage qui peut aussi jouer de la musique. Tout ça se met sur
  la ceinture. Je crois que ça offre un peu plus de flexibilité pour les proprié-
  taires. Donc :
  " Avec l’aide d’un dispositif audio conçu pour les guides touristiques, André
  Chapatte rassemblera le publique intéressé au Maga (vraisemblablement
  à une heure annoncée) et les emmènera au Snack-bar partenaire afin d’y
  présenter un élément de son spectacle. Selon la disponibilité des snacks
  partenaires, il se peut que le spectacle se déroule dans plusieurs lieux."
  C.Q : Tu as le don d'ubiquité ?
  A.D.C : :)))

  17/12/16- 13h- Repas chez moi au 73A rue Pierre Decoster. Potirons grillés
  au four et son dhal de lentille coco épicé à l'indienne, thé, chocolat marzi-
  pan amaretto.
  André D. Chapatte : Je ne sais pas si je peux parler de mon travail comme
  ça. Résumer son travail c'est toujours quelque chose de très difficile. Je
  crois que j'ai envie de laisser les gens faire leur propre interprétation du
  contenu de ce que je propose.
  Charlie Qasimov : Je comprends. C'est pour ça que je propose une forme de
  dialogue ouvert sur le travail, une forme d'entretien permanent. Je pense
  que par le dialogue, il y a beaucoup de compréhension qui peut émerger à
  la fois pour celui qui crée et celui qui reçoit.
  Qu'est ce que tu as prévu pendant ton spectacle, tu danses, tu chantes ou
  tu parles ?
  A.D.C: Oui il y a de ça. Je veux surtout tenter de créer un lien entre le public
  et ce que je propose. Dans le dernier spectacle, j'en suis venu assez naturel-
  lement à parler du rejet. Qui rejette qui ? De rejeter quelqu'un par amour.
  De rejeter une certaine forme de violence. Je propose quelque chose de très
  bizarre. Je porte un wonderbra rose de sport à l'envers, ce qui souligne les
  pectoraux et par dessus un cuir en passant moi-même par des états assez
  archetypé ce que signifie devoir être un homme fort.
  C.Q : Quels sont les outils ou les accessoires que tu utilises sur scène pour
  appuyer ton récit ?
  A.D.C : J'ai un microphone, des cigarettes avec une goutte de sang dessus
  que je distribue au public, la veste en cuir, mon smartphone, une couteau
  à cran, le wonderbra. J'utilise le microphone ou je ne l'utilise pas selon les
  moments. Pour l'instant je n'ai pas ajouté de décor comme ce que je faisais
  précedemment.
  C.Q: Ca me fait penser à ce film américain que je regardais quand j'étais
  mome, " My girl ". A un moment, les deux gosses, amis et amoureux, font un
  pacte de sang en se faisant une entaille dans la main afin de mélanger leur
  sang et de s'appartenie l'un et l'autre à tout jamais. Ici tu donnes un peu de
  ton sang au public.
  A.D.C: Je peux penser à ce que veut dire ce symbole en effet.
  C.Q: Est ce que tu veux faire un spectacle de ce genre dans le snack ?
  A.D.C: Non, pas vraiment. Je veux que ce soit en français. Dans ce que je
  décris, je parle surtout en anglais avec certains moments en français.
  Au téléphone, 12/01//17
  A.D.C : Tu saurais me donner les dates de l'évènement au Maga ?
  C.Q: Oui ce sera le week end de 21/22/23 avril 2017 en temps forts, puis
  des évènements toute la semaine suivante jusqu'au dimanche 30 avril de
  manière plus éparses. Ce serait bien que tous les spectacles/shows tiennent
  le même week end et que ce soit rejoué si il faut en semaine, je ne sais pas
  encore comment je vais répartir ce que chacun fait temporellement.
  J'ai envie qu'il y ait un contraste entre des pratiques de performances assez
  fortes, assumés au niveau de la présence corporelle par des spectacles à
  travailler dans l'espace avec des formes beaucoup plus discrètes, quasi invi-
  sibles et que ces deux choses tiennent ensembles au maga.
  A.D.C : Je pense à construire quelque chose, une scène pour l'occasion, à
  l'intérieur du maga, comme un point d'accroche qui permet aux visiteurs
  d'avoir quelque chose à regarder même quand le spectacle n'est plus là. Tra-
  vailler avec cette idée de générosité, de visibilité.
  A.D.C: Dans le dernier spectacle que j'ai monté, je recherche une forme
  hybride. Je danse en chantant, je chante juste, je parle quand je chante. Je
  switch d'un moment que je répète beaucoup et qui est maitrisé où le "per-
  sonnage" parle au public, à des moments de relachements où je suis "juste
  moi". C.Q: Je suis complètement pour cette idée d'être généreux et qu'il y ait des
  contrepoints matériels dans l'espace en contraste avec des moments forts
  de présences. Si les gens se déplacent et qu'ils ont loupé un spectacle, une
  scène ouverte, un moment d'échange, il y aura quand même quelque chose à
  voir, une trace, quelque chose à percevoir et à lire.
  C.Q: Tu veux dire que tu joues entre les codes scéniques du maintien et
  ceux du relachement des coulisses pendant le spectacle ? A.D.C: Qui sera présent alors pour cet évènement ?
<!--
  
  C.Q : Pour le moment, j'ai eu des rdv avec Coraline, Xavier et Rares, Marine,
  POP et Koen, Romain de l'atelier bek, tout le monde est partant. Il me reste à
  boire un café avec Jot Fau pour voir ce qu'elle pourrait produire en sculpture
  et discuter avec Emilie de son intervention universitaire. Je n'ai juste aucune
  réponse de Otto pour le moment.
  A.D.C : Ici, tu vas vraiment curaté l'exposition ?
  C.Q: Oui, dans le sens premier du terme, c'est à dire, prendre soin des gens
  et des projets des gens que j'invite. D'ici un mois, je vais commencer à orga-
  niser des rdv collectifs avec tous les participants pour créer des passerelles,
  que les participants puissent se rencontrer en avance. J'attends que chacun
  soit plus au clair avec ses propositions et que la thématique soit bien lancée
  dans les esprits de chacun.
  A.D.C: Je vais contacter Otto maintenant pour lui demander ce qu'il pourrait
  faire pour cet évènement. Je pense qu'il peut avoir une bourse de Finlande
  pour être défrayée du vol.
  C.Q: Super, c'est bien si tu t'occupes de Otto, au niveau de l'échange, ce sera
  plus facile entre vous deux. Tu peux déjà lui raconter pas mal de choses.
  Le 18/01/2017, 73 A rue Pierre Decoster, radis noir en rondelle, omelette
  aux pleurottes & shitakés, persil tubéreux, ail et salade de pâte, vin rouge.
  Otto est en Finlande. Il a déjà prit ses tickets d'avion pour venir à Bruxelles
  en avril. Avec André Chapatte, nous devons lui rédiger une lettre d'invita-
  tion en anglais pour le défraiement de son vol. Ainsi :
  Dear Otto Byström,
  You are invited to perform during the festival « Utopias and Convivial Spaces
  » between the 21st and the 30th of April 2017, at the exhibition space Maga
  (avenue Jean-Volders 56, St-Gilles, Bruxelles).
  Cordially,
  Curator of the festival :
  Stéphanie Verin
  Responsible of the Maga :
  Chloé Schuitten
  Nous rédigeons cette lettre à 23h30, buvant un dernier verre de vin en
  essayant de lier plaisir et efficacité à cette contrainte administrative.
  Au début du repas, nous commençons par questionner les relations homme/
  femme. Sujet classique entre André Chapatte et moi. Je lance le débat: Est
  ce que ça vaut la peine d'être autonome au point de rejeter certaines valeurs
  traditionnelles que peut apporter un homme dans une relation ? Je lui dis
  que j'en viens presque à jouer certains comportements dans mes relations
  pour être tranquilles et maintenir un bon équilibre dans l'intimité. Je ne sais
  pas vraiment si c'est ce que je veux profondément. André parle de ce malaise
  qu'il ressent quand une femme lui demande trop de soutien. Je parle du
  malaise que je ressens quand on me demande de m'appuyer sur/ ou d'être
  soutenue pour avoir une valeur de "femme" qui a besoin d'un homme dans
  une relation. On parle du "Maca", une plante qui régularise les hormones
  mâles et femelles. Je lui dis: "Sait tu que certains textes traditionnelles disent
  qu'une femme ne doit pas être stressée pour accueillir un homme. Elle reste
  à la maison, elle prépare tout pour le confort de l'homme et la bonne
  entente dans la sexualité est maintenue." C'est évidemment réducteur de
  penser une vie de femme ainsi aujourd'hui, mais certains dysfonctionne-
  ments sexuels viennent très certainement d'une sur-activité des femmes
  au niveau de leur travail. André me parle de sa vision d'une femme qui
  doit tout tenir dans sa vie au niveau de son travail, de ses relations sociales
  mais aussi du soin apporté à la maison, aux enfants. Il me dit: " On voit
  encore beaucoup d'homme ne pas prendre soin d'eux , de leur environne-
  ment et ne s'axer que sur un travail, une mission extérieur à leur environ-
  nement domestique. Mais c'est difficile aussi en tant qu'homme de rentrer
  dans "les critères" des femmes indépendantes qui justement veulent un
  homme qui se prend en main pour tout."
  Puis nous parlons du travail de André et de la façon dont il a envie de
  transmettre son histoire le temps du festival. Il pense construire deux
  scènes à l'intérieur du Maga et amener le spectateur à réaliser une boucle
  de la haute sphère à la basse sphère, mêlant les milieux au sein d'une
  même performance. Il veut travailler la sculpture de manière plus précise
  pour ce travail et va demander de collaborer avec un ami et un ancien
  étudiant de la Riesvelt Academy qui produisait un travail fin de sculpture
  et qui savait rendre "noble" d'apparence des materiaux simples comme
  le bois. Il me parle de sculpture à la Donald Duck, des sortes de sculp-
  tures absurde qui trônent débilement dans les espaces publiques des B.D,
  ridiculisant l'art prétentieux des maitres. Je lui soumet l'idée de travail-
  ler avec de nouveaux matériaux et de peut-être mélanger les genres de
  son travail au sein même de l'objet. André hésite à créer un spectacle du
  Zachary Airhorn Show qui est plus directement pop et musical ou bien un
  spectacle qui mêle musique et performance. Je lui fais remarquer que ses
  objets appartiennent soit à un monde soit à un autre mais que si il hésite
  entre ces deux formes c'est qu'il peut peut-être faire fusionner les types de
  spectacle en une troisième forme de spectacle.
  Il me dessine des scènes cassés, qui selon son vocabulaire, veulent signi-
  fier qu'il ne veut pas en tant qu'artiste se figer dans le monde du spectacle.
  Je lui parler de podiums brillants, laqués noir, à la gogo tales dans le film
  de Ferrara. Je lui propose de reprendre les codes du glamour marchand
  qui correspond à l'univers dépeint dans ses performance plus minimales :
  André Chapatte et sa présence sportive.
  Il a vraiment envie de rentrer en contact avec un des gars du snacks en
  face du maga le plus rapidement possible. Il hésite à imprimer son portfo-
  lio pour montrer son travail. On se dit tous les deux que les conversations
  informels sont toujours les meilleures et ce sont celles qui engagent le plus
  de contact chaleureux avec les gens. Alors il n'y a plus qu'à y aller, manger
  des frites et parler. Il me dit :
  " J'arriverai juste comme un gars qui leur raconte que je fais des spec-
  tacles. J'aimerais offrir des yahourts au public, faire fonctionner leur com-
  merce. Je veux vraiment raconter l'histoire de ces types, construire ma
  narration en fonction de ce qu'ils vont me raconter. Peut-être donner en
  titre à cette chanson le nom du snack, tout simplement. Etre aussi direct
  et simple."
  Nous écoutons "Salade, tomate, oignon. " du groupe de musique La
  caravane passe. André me fait rire avec cette référence ultra populaire qui
  fonctionne à la fois auprès des kébabs et du public.
  " Mais que veut faire Otto ?"
  — Beaucoup de bruit avec une guitare éléctrique. Otto vient de la culture
  grind/death métal à la base. Il est plutôt anti-utopiste dans sa position.
  Il se moque des espoirs naïfs et des révolutions utopistes contem-
  poraines. Il est touché par les travaux d'artistes qui parlent de la
  dépression que provoque notre monde capitaliste actuel. "
  Nous parlons du travail de Ed Atkins. André trouve son travail
  très bon mais peut être avec un degré en moins d'humanité pour
  transcender les sphères culturelles. Je lui dis que Ed Atkins trans-
  pire la mélancolie contemporaine et qu'il n'a pas besoin d'être plus
  généreux. J'aime le fait que son travail soit chrypté. Cependant,
  j'avais été subjuguée par la générosité de son installation au Ste-
  delijk Museum d'Amsterdam et cependant déçue par la présence
  d'une seule vidéo en "stéréo" dans la galerie dépendance à Brussel,
  comme si son œuvre ne pouvait se lire avec satisfaction qu' en gar-
  dant des contenus diversifiés proches les uns des autres. La noirceur
  bruyante de Otto est bienvenue, reste à savoir quand et comment
  l'intégrer, à quelle heure, prévenir le voisinage qu'un Finlandais qui
  veut faire du bruit vient se produire à Bruxelles. André me raconte
  de manière brève l'histoire de la Finlande qui a été colonisée par la
  Suède jusqu'à la seconde-guerre mondiale. Il me dit que le peuple
  finnois a gardé en mémoire traumatique cet épisode collonial et qu'il
  y a une tendance là-bas à vendre facilement les productions artis-
  tiques. La valeur poétique des œuvres est rapidement détruite par
  le système économique puissant. Otto et ses collaborateurs, avec la
  galerie Sorbus, prennent une position satyrique vis-à-vis du marché.
  Quelle valeur pouvons nous détourner au prix d'un bol de corn flakes
  ? Quelle valeur pouvons nous donner aujourd'hui à des symboles
  anarchistes récupérés par les médias ? La poésie n'a pas de prix, c'est
  bien connu, mais nous avons tendance à l'oublier instantanément.
  On finit par parler de typographie et à se moquer de la Minion Pro de
  In Design. Mon logiciel n'a ni la Futura, ni la Helvetica. André trouve
  une typographie dynamique et honteusement formelle pour écrire la
  lettre d'invitation, la Modern N°20. -->
</div>
