<div class="contentcontent">

	<div class="titre"><span>Poisson-Évêque</span> <span>21 → 30 avril 2017</span></div>
<!--
	<div class="date">

	</div> -->


	<div class="people">
		<!-- <p class="curatrice"><b>Curatrice&thinsp;:</b>
			Un festival organisé par Stéphanie Vérin.
		</p> -->
		<ul>
			<li class="list1"><span>Stéphanie Becquet</span> <span>Stéphanie Becquet</span></li>
			<li class="list2"><span>Stephan Blumenschein</span> <span>Stephan Blumenschein</span></li>
			<li class="list3"><span>Otto Byström</span> <span>Otto Byström</span></li>
			<li class="list4"><span>André D.Chapatte</span> <span>André D.Chapatte</span></li>
			<li class="list5"><span>Brique Corps</span> <span>Brique Corps</span></li>
			<li class="list6"><span>Rares Craiut</span> <span>Rares Craiut</span></li>
			<li class="list7"><span>Jot Fau</span> <span>Jot Fau</span></li>
			<li class="list8"><span>Irina Favero-Longo</span> <span>Irina Favero-Longo</span></li>
			<li class="list9"><span>Xavier Gorgol</span> <span>Xavier Gorgol</span></li>
			<li class="list10"><span>Marjolein Guldentops</span> <span>Marjolein Guldentops</span></li>
			<li class="list11"><span>Marine Kaiser</span> <span>Marine Kaiser</span></li>
			<li class="list12"><span>Maxime Lacôme</span> <span>Maxime Lacôme</span></li>
			<li class="list13"><span>Antoine Loyer</span> <span>Antoine Loyer</span></li>
			<li class="list14"><span>Sandra Naji</span> <span>Sandra Naji</span></li>
			<li class="list15"><span>Clément Thiry</span> <span>Clément Thiry</span></li>
			<li class="list16"><span>Rebecca Konforti</span> <span>Rebecca Konforti</span></li>
			<li class="list17"><span>Bettina Kordjani</span> <span>Bettina Kordjani</span></li>
			<li class="list18" style="text-align:left;"><span>Potential Office Project</span><span>Potential Office Project</span></li>
			<li class="list19"><span>Romy Vanderveken</span> <span>Romy Vanderveken</span></li>
			<li class="list20"><span>Stéphanie Verin</span> <span>Stéphanie Verin</span></li>
		</ul>
	</div>

	<p class="lieu">
		<span>Le Maga</span>
		<span>Avenue Jean Volders, 56</span><br>
		<span>1060 Saint-Gilles Belgique</span>
	</p>

</div>

<div class="contentcontentVerso1">

	<div class="titre"><span>Poisson-Évêque</span> <span>21 → 30 avril 2017</span></div>
<!--
	<div class="date">

	</div> -->


	<div class="people">
		<!-- <p class="curatrice"><b>Curatrice&thinsp;:</b>
			Un festival organisé par Stéphanie Vérin.
		</p> -->
		<ul>
			<li class="list1"><span>Becquet</span> <span>Becquet</span></li>
			<li class="list2"><span>Blumenschein</span> <span>Blumenschein</span></li>
			<li class="list3"><span>Byström</span> <span>Byström</span></li>
			<li class="list4"><span>D.Chapatte</span> <span>D.Chapatte</span></li>
			<li class="list5"><span>Corps</span> <span>Corps</span></li>
			<li class="list6"><span>Craiut</span> <span>Craiut</span></li>
			<li class="list7"><span>Fau</span> <span>Fau</span></li>
			<li class="list8"><span>Favero-Longo</span> <span>Favero-Longo</span></li>
			<li class="list9"><span>Gorgol</span> <span>Gorgol</span></li>
			<li class="list10"><span>Guldentops</span> <span>Guldentops</span></li>
			<li class="list11"><span>Kaiser</span> <span>Kaiser</span></li>
			<li class="list12"><span>Lacôme</span> <span>Lacôme</span></li>
			<li class="list13"><span>Loyer</span> <span>Loyer</span></li>
			<li class="list14"><span>Naji</span> <span>Naji</span></li>
			<li class="list15"><span>Thiry</span> <span>Thiry</span></li>
			<li class="list16"><span>Konforti</span> <span>Konforti</span></li>
			<li class="list17"><span>Kordjani</span> <span>Kordjani</span></li>
			<li class="list18" style="text-align:left;"><span>Office Project</span> <span>Office Project</span></li>
			<li class="list19"><span>Vanderveken</span> <span>Vanderveken</span></li>
			<li class="list20"><span>Verin</span> <span>Verin</span></li>
		</ul>
	</div>

	<p class="lieu">
		<span>Le Maga</span>
		<span>Avenue Jean Volders, 56</span><br>
		<span>1060 Saint-Gilles Belgique</span>
	</p>

</div>

<div class="marge margeHoriz margeTop"></div>
<div class="marge margeHoriz margeBottom"></div>
<div class="marge margeVert margeLeft"></div>
<div class="marge margeVert margeRight"></div>

<!-- <div id="gridVisible">
	<div class="rows">
		<div class="line horizontal row"></div>
	</div>
	<div class="cols">
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
	</div>
</div> -->

<!-- <img class="bgOr" src="img/Hobos2.jpg" alt="" /> -->

<div class="rectangle"></div>
