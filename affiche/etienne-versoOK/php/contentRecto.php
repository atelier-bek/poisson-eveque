<div id="eveque">

  <span id="title">
    <span><b>Festival</b>&nbsp;&nbsp;&nbsp;<em>Poisson―Évêque</em>Poisson―Évêque</span><br>
    <span class="dates">21 - 30 <b>avril 2017</b></span><br>
  </span>
  <div class="col1">
    <img src="css/img/formeL.png" class="shape shape1" alt="" />
    <img src="css/img/formeLR.png" class="shape shape2" alt="" />
    <p>
      Le <b>festival</b> Poisson-Évêque est une&nbsp;première édition regroupant vingt <b>artistes</b> internationaux. Pendant dix&nbsp;jours, nous proposerons une&nbsp;exposition, des&nbsp;récits musicaux, des&nbsp;lectures, des&nbsp;performances, des&nbsp;balades dans la&nbsp;ville, mais aussi des&nbsp;concerts. Nous&nbsp;questionnerons ensemble la&nbsp;gentrification, les&nbsp;féminismes émergeants, les&nbsp;effets sociaux du&nbsp;capitalisme
    et&nbsp;les&nbsp;manières de&nbsp;se&nbsp;réapproprier les&nbsp;outils techniques et&nbsp;technologiques contemporains. Vous serez conviés à&nbsp;découvrir les&nbsp;coins sombres de&nbsp;la&nbsp;ville de&nbsp;Bruxelles, à&nbsp;discuter autour de&nbsp;madeleines <span class="thinsp2">«&thinsp;</span>queer<span class="thinsp">&thinsp;</span>», à&nbsp;rencontrer nos&nbsp;faiseuses de&nbsp;soupe dans un&nbsp;appartement Saint-Gillois, à&nbsp;participer
    à&nbsp;des&nbsp;lectures publiques de&nbsp;bar en&nbsp;bar, à&nbsp;observer &nbsp;&nbsp;les&nbsp;sculptures de&nbsp;femmes &nbsp;&nbsp;&nbsp;&nbsp;et&nbsp;l’aménagement urbain et&nbsp;à&nbsp;vous &nbsp;&nbsp;abreuver d’un&nbsp;Ayran musical.
    Les événements du&nbsp;festival se&nbsp;dérouleront au&nbsp;<b>Maga</b>, lieu associatif situé dans le&nbsp;quartier très populaire de&nbsp;<b>Saint-Gilles</b> mais aussi
    dans le&nbsp;<b>snack<span class="letterspacing"> A</span>tlas</b>, dans le&nbsp;<b>bar&nbsp;Waterloo </b> et&nbsp;dans la&nbsp;ville de&nbsp;Bruxelles, afin de&nbsp;proposer une&nbsp;forme d’art social et&nbsp;utopiste engageant
  un&nbsp;dialogue avec la&nbsp;population présente.
    </p>
   </div>

    <span class="programme">
      <p class="prog">P r o g r a m m e</p><p class="progr">P r o g r a m m e</p>
    </span>
  <div class="col2">

    <h3>vendredi 21 avril<br></h3>
    <h3 style="box-shadow: inset 0px 0px 0px 1px rgba(0, 0, 0, 0); padding-left: 0; padding-right: 0; margin-left: 0; margin-right: 0;">O u v e r t u r e&nbsp;&nbsp;&nbsp;d e&nbsp;&nbsp;&nbsp;l ’ e x p o s i t i o n</h3>
    <ul>
      <li>
        <p class="dateType">
          <span class="date">18:00</span><span class="type">S c u l p t u r e ,&nbsp;&nbsp;&nbsp;P e r f o r m a n c e&nbsp;&nbsp;&nbsp;c u l i n a i r e ,&nbsp;&nbsp;&nbsp;I n s t a l l a t i o n</span>
          <span class="place">Le Maga</span>
        </p>
          <p class="people">
            Stephan<span class="space">&thinsp;&thinsp;</span>Blumenschein
            Passerby<span class="space">&thinsp;&thinsp;</span>(stages)<br>
            Xavier<span class="space">&thinsp;&thinsp;</span>Gorgol<span class="space">&thinsp;&thinsp;</span>&amp;<span class="space">&thinsp;&thinsp;</span>Rareş<span class="space">&thinsp;&thinsp;</span>Crăiuţ
            Transition
          </p>
        <p class="title"> </p>
          <span class="people">Jot<span class="space">&thinsp;&thinsp;</span>Fau</span>
      </li>

      <li>
        <p class="dateType">
          <span class="date">18:30</span><span class="type">p e r f o r m a n c e</span>
          <span class="place">Le Maga</span>
        </p>
        <p class="people">
          <span>Irina<span class="space">&thinsp;&thinsp;</span>Favero-Longo Posé<span class="space">&thinsp;&thinsp;</span>entre<span class="space">&thinsp;&thinsp;</span>le<span class="space">&thinsp;&thinsp;</span>coin</span><br>
          <span>&amp;<span class="space">&thinsp;&thinsp;</span>Rebecca<span class="space">&thinsp;&thinsp;</span>Konforti et<span class="space">&thinsp;&thinsp;</span>la<span class="space">&thinsp;&thinsp;</span>marche</span>
        </p>
      </li>

      <li>
        <p class="dateType">
          <span class="date">19:00</span><span class="type">P e r f o r m a n c e&nbsp;&nbsp;&nbsp;a v e c&nbsp;&nbsp;&nbsp;u n e&nbsp;&nbsp;&nbsp;g u i t a r e&nbsp;&nbsp;&nbsp;é l e c t r i q u e</span>
          <span class="place">
            Le Maga
          </span>
        </p>
        <p class="people">
          Otto<span class="space">&thinsp;&thinsp;</span>Byström
          Slump
        </p>

      </li>
      <li>
        <p class="dateType">
          <span class="date">20:00</span><span class="type">c h a n t&nbsp;&nbsp;&nbsp;d a n s e&nbsp;&nbsp;&nbsp;a y r a n</span>
          <span class="place">
            Le Maga &amp; Snack <span class="letterspacing"> A</span>tlas
          </span>
        </p>
        <p class="people">
          André<span class="space">&thinsp;&thinsp;</span>D.Chapatte Private<span class="space">&thinsp;&thinsp;</span>Utopia
        </p>
        <p class="title">

        </p>
      </li>
    </ul>
    <br>

    <h3>samedi 22 avril</h3>
    <ul>
     <li>
        <p class="dateType">
          <span class="date">18:00</span><span class="type">P e r f o r m a n c e&nbsp;&nbsp;&nbsp;c u l i n a i r e</span>
          <span class="place">
            Le Maga
          </span>
        </p>
        <p class="people">
          Xavier<span class="space">&thinsp;&thinsp;</span>Gorgol<span class="space">&thinsp;&thinsp;</span>&amp;<span class="space">&thinsp;&thinsp;</span>Rareş<span class="space">&thinsp;&thinsp;</span>Crăiuţ
          Transition
        </p>
      </li>
     <li>
      <p class="dateType">
        <span class="date">20:00</span><span class="type">r é c i t&nbsp;&nbsp;&nbsp;m u s i c a l</span>
      <span class="place">
        Le Maga
      </span>
      </p>
      <p class="people">
        Sandra<span class="space">&thinsp;&thinsp;</span>Naji<span class="space">&thinsp;&thinsp;</span>
        Rituel<span class="space">&thinsp;&thinsp;</span>des<span class="space">&thinsp;&thinsp;</span>vieilles<br>
        &amp;<span class="space">&thinsp;&thinsp;</span>Antoine<span class="space">&thinsp;&thinsp;</span>Loyer
        de<span class="space">&thinsp;&thinsp;</span>Bredene-Aan-Zee
      </p>
    </li>
        <li>
          <p class="dateType">
            <span class="date">21:00</span><span class="type">c h a n t&nbsp;&nbsp;&nbsp;d a n s e&nbsp;&nbsp;&nbsp;a y r a n</span>
            <span class="place">
              Le Maga &amp; Snack <span class="letterspacing"> A</span>tlas
            </span>
          </p>
          <p class="people">
            André<span class="space">&thinsp;&thinsp;</span>D.Chapatte Private<span class="space">&thinsp;&thinsp;</span>Utopia
          </p>
        </li>
      </li>
      <li>
        <p class="dateType">
          <span class="date">22:00</span><span class="type">P e r f o r m a n c e&nbsp;&nbsp;&nbsp;a v e c&nbsp;&nbsp;&nbsp;u n e&nbsp;&nbsp;&nbsp;g u i t a r e&nbsp;&nbsp;&nbsp;é l e c t r i q u e</span>
          <span class="place">
            Bar Waterloo
          </span>
        </p>
        <p class="people">
          Otto<span class="space">&thinsp;&thinsp;</span>Byström
          Slump
        </p>
      </li>
      <li>
         <p class="dateType">
          <span class="date">22:30</span><span class="type">c o n c e r t&nbsp;&nbsp;&nbsp;r o c k&nbsp;&nbsp;&nbsp;p o s t&nbsp;&nbsp;&nbsp;m o d e r n e</span>
          <span class="place">
            Bar Waterloo
          </span>
        </p>
        <p class="people">
          André<span class="space">&thinsp;&thinsp;</span>D.Chapatte
          The<span class="space">&thinsp;&thinsp;</span>Zachary<span class="space">&thinsp;&thinsp;</span>Airhorn<span class="space">&thinsp;&thinsp;</span>Show
        </p>
      </li>
      <li>
        <p class="dateType">
          <span class="date">23:00</span><span class="type">c o n c e r t&nbsp;&nbsp;&nbsp;T e c h n o&nbsp;&nbsp;&nbsp;e x p é r i m e n t a l e</span>
        <span class="place">
          Bar Waterloo
        </span>
        </p>
        <p class="people">
          Maxime<span class="space">&thinsp;&thinsp;</span>Lacôme
          Minor<span class="space">&thinsp;&thinsp;</span>mishap
        </p>
      </li>
      <li>
              <p class="dateType">
                <span class="date">00:00</span><span class="type">M i x&nbsp;B r e a k c o r e</span>
                <span class="place">
                  Bar Waterloo
                </span>
              </p>
              <p class="people">
                Brique<span class="space">&thinsp;&thinsp;</span>Corps
              </p>
            </li>

    </ul>
  </div>

  <div class="col3">
    <ul>
        <li>
          <p class="dateType">
            <span class="date">01:30</span><span class="type">c h r o n i q u e&nbsp;&nbsp;&nbsp;m u s i c a l e&nbsp;&nbsp;&nbsp;f é m i n i s t e</span>
          <span class="place">
            Bar Waterloo
          </span>
          </p>
          <p class="people">
            Bettina<span class="space">&thinsp;&thinsp;</span>Kordjani
            Pussies<span class="space">&thinsp;&thinsp;</span>grab<span class="space">&thinsp;&thinsp;</span>back
          </p>
          <p class="title">
            ✚<span class="space">&thinsp;&thinsp;</span>des<span class="space">&thinsp;&thinsp;</span>surprises<span class="space">&thinsp;&thinsp;</span>pour<span class="space">&thinsp;&thinsp;</span>danser<span class="space">&thinsp;&thinsp;</span>toute<span class="space">&thinsp;&thinsp;</span>la<span class="space">&thinsp;&thinsp;</span>nuit
          </p>
        </li>

    </ul>
    <br>
    <h3 style="margin-top: 0.5mm;">dimanche 23 avril</h3>
    <ul>
      <li>
        <p class="dateType">
          <span class="date">16:00-19:00</span><span class="type">b a l a d e</span>
        <span class="place">
          rdv Centre Culturel de Uccle
        </span>
        </p>
        <p class="people">
          Marine<span class="space">&thinsp;&thinsp;</span>Kaiser<span class="space">&thinsp;&thinsp;</span>
          La<span class="space">&thinsp;&thinsp;</span>Ballade<span class="space">&thinsp;&thinsp;</span>de<br>
          &amp;<span class="space">&thinsp;&thinsp;</span>Stéphanie<span class="space">&thinsp;&thinsp;</span>Verin
          Kaiser<span class="space">&thinsp;&thinsp;</span>et<span class="space">&thinsp;&thinsp;</span>Qasimov
        </p>
      </li>
      <li>
        <p class="dateType">
          <span class="date">00:00-01:30</span><span class="type">b a l a d e</span>
        <span class="place">
           rdv par mail&thinsp;: <span class="letterspacing">zdero</span>uzze@gmail.com</span>
        </span>
        </p>
        <p class="people">
          Clément<span class="space">&thinsp;&thinsp;</span>Thiry
          Visite<span class="space">&thinsp;&thinsp;</span>des<span class="space">&thinsp;&thinsp;</span>coins<span class="space">&thinsp;&thinsp;</span>sombres<span class="space">&thinsp;&thinsp;</span>de<span class="space">&thinsp;&thinsp;</span>Bruxelles
        </p>
      </li>
    </ul>
    <br>

    <h3>lundi 24 avril</h3>
    <ul>
       <li>
        <p class="dateType">
          <span class="date">00:00-01:30</span><span class="type">b a l a d e</span>
        <span class="place">
           rdv par mail&thinsp;: <span class="letterspacing">zdero</span>uzze@gmail.com</span>
        </span>
        </p>
        <p class="people">
          Clément<span class="space">&thinsp;&thinsp;</span>Thiry
          Visite<span class="space">&thinsp;&thinsp;</span>des<span class="space">&thinsp;&thinsp;</span>coins<span class="space">&thinsp;&thinsp;</span>sombres<span class="space">&thinsp;&thinsp;</span>de<span class="space">&thinsp;&thinsp;</span>Bruxelles
        </p>
      </li>
    </ul>
    <br>

    <h3>mardi 25 avril</h3>
    <ul>
       <li>
        <p class="dateType">
          <span class="date">00:00-01:30</span><span class="type">b a l a d e</span>
        <span class="place">
          rdv par mail&thinsp;: <span class="letterspacing">zdero</span>uzze@gmail.com</span>
        </span>
        </p>
        <p class="people">
          Clément<span class="space">&thinsp;&thinsp;</span>Thiry
          Visite<span class="space">&thinsp;&thinsp;</span>des<span class="space">&thinsp;&thinsp;</span>coins<span class="space">&thinsp;&thinsp;</span>sombres<span class="space">&thinsp;&thinsp;</span>de<span class="space">&thinsp;&thinsp;</span>Bruxelles
        </p>
      </li>
    </ul>
    <br>

    <h3 style="margin-top: 0.5mm;">mercredi 26 avril</h3>
    <ul>
      <li>
        <p class="dateType">
          <span class="date">19h30</span><span class="type">B a l a d e &nbsp;&nbsp;d e&nbsp;&nbsp; b a r&nbsp;&nbsp; e n&nbsp;&nbsp; b a r&nbsp;&nbsp; e t&nbsp;&nbsp; l e c t u r e s</span>
        <span class="place">
          du Maga au <span class="letterspacing">Midpo</span>int Café
        </span>
        </p>
        <p class="people">
          POP&thinsp;(Potential&thinsp;Office&thinsp;Project)
        </p>
      </li>
       <li>
        <p class="dateType">
          <span class="date">00:00-01:30</span><span class="type">b a l a d e</span>
          <span class="place">
            départ&thinsp;: Mid<span class="letterspacing">po</span>int Café
          </span>
        </p>
        <p class="people">
          Clément<span class="space">&thinsp;&thinsp;</span>Thiry
          Visite<span class="space">&thinsp;&thinsp;</span>des<span class="space">&thinsp;&thinsp;</span>éclairages<span class="space">&thinsp;&thinsp;</span>défectueux
        </p>
      </li>
      <br>
      <h3>jeudi 27 avril</h3>
      <ul>
        <li>
          <p class="dateType">
            <span class="date">18:00</span><span class="type">P e r f o r m a n c e&nbsp;&nbsp; c u l i n a i r e</span>
            <span class="place">
              Chaussée de Forest 106, S<sup>t</sup> Gilles
            </span>
           </p>
           <p class="people">
              Stéphanie<span class="space">&thinsp;&thinsp;</span>Becquet,<span class="space">&thinsp;&thinsp;</span>Marjolein
              Kitchen<span class="space">&thinsp;&thinsp;</span>Session<br>
              Guldentops<span class="space">&thinsp;&thinsp;</span>&amp;<span class="space">&thinsp;&thinsp;</span>Romy<span class="space">&thinsp;&thinsp;</span>Vanderveken
          </p>
        </li>
      </ul>
      <br>

  </div>

  <div class="col4">
    <h3>vendredi 28 avril</h3>
    <ul>
      <li>
        <p class="dateType">
          <span class="date">10:00-13:00</span><span class="type">b a l a d e</span>
        <span class="place">
          rdv Centre Culturel de Uccle
        </span>
        </p>
        <p class="people">
          Marine<span class="space">&thinsp;&thinsp;</span>Kaiser<span class="space">&thinsp;&thinsp;</span>
          La<span class="space">&thinsp;&thinsp;</span>Ballade<span class="space">&thinsp;&thinsp;</span>de<br>
          &amp;<span class="space">&thinsp;&thinsp;</span>Stéphanie<span class="space">&thinsp;&thinsp;</span>Verin
          Kaiser<span class="space">&thinsp;&thinsp;</span>et<span class="space">&thinsp;&thinsp;</span>Qasimov
        </p>
      </li>
            <li>
              <p class="dateType">
                <span class="date">18:00</span><span class="type">P e r f o r m a n c e&nbsp;&nbsp; c u l i n a i r e</span>
                <span class="place">
                  Chaussée de Forest 106, S<sup>t</sup> Gilles
                </span>
               </p>
               <p class="people">
                  Stéphanie<span class="space">&thinsp;&thinsp;</span>Becquet,<span class="space">&thinsp;&thinsp;</span>Marjolein
                  Kitchen<span class="space">&thinsp;&thinsp;</span>Session<br>
                  Guldentops<span class="space">&thinsp;&thinsp;</span>&amp;<span class="space">&thinsp;&thinsp;</span>Romy<span class="space">&thinsp;&thinsp;</span>Vanderveken
              </p>
            </li>
    </ul>

    <ul>
      <li>
        <p class="dateType">
          <span class="date">18:00</span><span class="type">P e r f o r m a n c e&nbsp;&nbsp; c u l i n a i r e</span>
          <span class="place">
            Chaussée de Forest 106, S<sup>t</sup> Gilles
          </span>
         </p>
         <p class="people">
            Stéphanie<span class="space">&thinsp;&thinsp;</span>Becquet,<span class="space">&thinsp;&thinsp;</span>Marjolein
            Kitchen<span class="space">&thinsp;&thinsp;</span>Session<br>
            Guldentops<span class="space">&thinsp;&thinsp;</span>&amp;<span class="space">&thinsp;&thinsp;</span>Romy<span class="space">&thinsp;&thinsp;</span>Vanderveken
        </p>
      </li>
  </ul>
    <h3>dimanche 30 avril</h3>
    <ul>
      <li>
     <p class="dateType">
        <span class="date">18:00</span><span class="type">L a n c e m e n t&nbsp;&nbsp; d u&nbsp;&nbsp; l i v r e</span>
      <span class="place">Le Maga</span>
      </p>
        <p class="people">Romain<span class="space">&thinsp;&thinsp;</span>Marula,<span class="space">&thinsp;&thinsp;</span>Étienne<span class="space">&thinsp;&thinsp;</span>Ozeray
          Poisson-évêque<br>
          &amp;<span class="space">&thinsp;&thinsp;</span>Stéphanie<span class="space">&thinsp;&thinsp;</span>Verin
      </p>
      </li>

      <li>
        <p class="dateType">
          <span class="date">19:00</span><span class="type">p e r f o r m a n c e</span>
          <span class="place">Le Maga</span>
        </p>
        <p class="people">
          <span>Irina&thinsp;Favero-Longo Posé<span class="space">&thinsp;&thinsp;</span>entre<span class="space">&thinsp;&thinsp;</span>le<span class="space">&thinsp;&thinsp;</span>coin</span><br>
          <span>&amp;<span class="space">&thinsp;&thinsp;</span>Rebecca<span class="space">&thinsp;&thinsp;</span>Konforti et<span class="space">&thinsp;&thinsp;</span>la<span class="space">&thinsp;&thinsp;</span>marche</span>
        </p>
      </li>

    </ul>
    <br>

<div class="credits">
     <p class="lieu">
      Le Maga <b>Avenue Jean Volders, 56 </b> 1060 Saint-Gilles  <br>
      Bar Waterloo <b>Chaussée de Waterloo, 217 </b> 1060 Saint-Gilles  <br>
      Snack Atlas <b>Parvis de <span class="letterspacing">Saint-Gill</span>es, 6 </b> 1060 Saint-Gilles  <br>

      Midpoint Café <b>Rue de Fland<span class="letterspacing">res</span>, 189 </b> 1000 Bruxelles<br>
      <p>
    <br>
        Un festival organisé par&thinsp;<b>Stéphanie&thinsp;<span class="letterspacing">Ve</span>rin</b>, assistée d'<b>Émilie Salson</b>.
        <br>Ce programme-affiche-couverture a été mis en forme par <br><b>Romain Marula</b> &amp; <b>Étienne<span class="letterspacing"> Ozer</span>ay</b>
        en Html et Css, imprimé en riso chez&thinsp;<b>Ronan Deriez</b> sur  du papier <b>Arcoprint Milk 85g</b>. Les caractères
        typographiques utilisés sont <b>Candal</b> et <b>Computer <span class="letterspacing">Mo</span>dern</b>, tout <br>deux sous licence libre.<br>
        <b>www.tiny.cc/poissoneveque</b>
      </p>
      </div>
</div>
</div>

<div id="poisson">

</div>
</div>
