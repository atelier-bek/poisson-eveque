<?php
  include('inc/variables.php');
  include('inc/head.php');
?>

<div id="pageRecto" class="page">
  <div class="grids">
    <div class="bookFolds grid">
      <div class="leftFold horizontal line"></div>
      <div class="horizontalCenter horizontal line"></div>
      <div class=" rightFold horizontal line"></div>
      <div class="topFold vertical line"></div>
      <div class="bottomFold vertical line"></div>
    </div>
    <div class="flyerFolds grid">
      <div class="topFold horizontal line"></div>
      <div class="horizontalCenter horizontal line"></div>
      <div class="bottomFold horizontal line"></div>
      <div class="verticalFold vertical line"></div>
    </div>
    <div class="theGrid grid">
      <div class="rows">
        <?php
          for ($i=1; $i <= 40; $i++) {
            ?>
            <div class="row row<?= $i ?> horizontal line"></div>
            <?php
          }
        ?>
      </div>
      <div class="cols">
        <?php
          for ($i=1; $i <= 37; $i++) {
            ?>
            <div class="col col<?= $i ?> vertical line"></div>
            <?php
          }
        ?>
      </div>
    </div>
  </div>
</div>
<div id="contentRecto" class="content">
  <?php include('php/contentRecto.php'); ?>
</div>
<div id="pageVerso" class="page">
  <div class="grids">
    <div class="bookFolds grid">
      <div class="leftFold horizontal line"></div>
      <div class="horizontalCenter horizontal line"></div>
      <div class=" rightFold horizontal line"></div>
      <div class="topFold vertical line"></div>
      <div class="bottomFold vertical line"></div>
    </div>
    <div class="flyerFolds grid">
      <div class="topFold horizontal line"></div>
      <div class="horizontalCenter horizontal line"></div>
      <div class="bottomFold horizontal line"></div>
      <div class="verticalFold vertical line"></div>
    </div>
    <div class="theGrid grid">
      <div class="rows">
        <?php
          for ($i=1; $i <= 40; $i++) {
            ?>
            <div class="row row<?= $i ?> horizontal line"></div>
            <?php
          }
        ?>
      </div>
      <div class="cols">
        <?php
          for ($i=1; $i <= 37; $i++) {
            ?>
            <div class="col col<?= $i ?> vertical line"></div>
            <?php
          }
        ?>
      </div>
    </div>
  </div>
</div>
<div id="contentVerso" class="content">
  <?php include('php/contentVerso.php'); ?>
</div>

<?php include('inc/foot.php'); ?>
