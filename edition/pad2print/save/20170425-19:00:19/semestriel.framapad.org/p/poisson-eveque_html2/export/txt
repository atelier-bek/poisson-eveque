<!--documentation-->


<!-- PAGE -->
<!-- [6-7] -->

<!-- BREAK -->

<h2 class="type">P e r f o r m a n c e</h2>
<h2 class="nom">Irina Favero-Longo <br>&amp; Rebecca Konforti</h2>
<h2> Posé entre le coin et la marche</h2>
<!--
<h2 class="nom">Irina<span class="space">&thinsp;&thinsp;</span>Favero-Longo Posé<span class="space">&thinsp;&thinsp;</span>entre<span class="space">&thinsp;&thinsp;</span>le<span class="space">&thinsp;&thinsp;</span>coin<br>          
&amp;<span class="space">&thinsp;&thinsp;</span>Rebecca<span class="space">&thinsp;&thinsp;</span>Konforti et<span class="space">&thinsp;&thinsp;</span>la<span class="space">&thinsp;&thinsp;</span>marche</h2>
-->
<br>
<p class="first">La forme de mon cube à trous vient d’une image mentale, une utopie dans laquelle l’humain et l’architecture fusionnent: une créature hybride entre corps humain et architecture. L’utopie subit une transformation quand elle est matérialisée et confrontée à une corporalité. Un mur à deux têtes, un cube à 4 pieds.</p>
<p>Ce sont des formes qui fonctionnent d’après une réciprocité: l’élément architectural accueille le corps et le corps donne vie à ce dernier. Les deux fonctionnent ensemble. Je construis alors des sculptures à trous qui seront manipulées par des corps. Elles sont structures à habiter et objets afonctionnels.</p>
<p>Objets lourds, gros, et inconfortables. Ces sculptures à trous sont aussi rendues «vivantes» par plusieurs personnes, ce qui implique un corps commun, dans lequel le rythme lent permet un déplacement fluide et une écoute de l’autre. C’est cet espace d’inconfort, celui d’une certaine gaucherie dans l’apprivoisement de ces sculptures par les corps qui pour moi crée un événement poétique.</p>
<p>Par les trous, les performeurs se montrent de manière fragmentée. Dans ces performances le corps alterne entre mouvements et postures. Entre mobilité et inertie. Dans les déplacements, c’est le corps qui agit sur la structure qu’il porte et lui donne vie, dans les pauses c’est la structure qui agit sur le corps. Le corps devient une partie de la sculpture, il se fond. Il devient un élément quasi-pictural, un ornement. On peut alors le scruter comme une œuvre. Cependant le fait qu’il s’agisse d’une sculpture humaine interroge</p>


<!-- PAGE -->
<!-- [8-9] -->
<p class="first"> la distance sociale: celle que garde le spectateur, qui est une autre forme de distance que la distance face à une œuvre.</p>
<p>Cette conscience d’humanité est aussi amenée par un aspect essentiel de ces sculptures: le double point de vue. Face à elles on voit des surfaces blanches et finies. De derrière on voit la structure intérieure, le bois brute, les éléments de construction et les corps qui tentent de la manipuler et de négocier avec. Ces deux faces sont toutes deux aussi importantes. Elles participent à la sculpture globale. L’envers du décor bricolé et chaotique est aussi beau que la face lisse. Sa présence est nécessaire.
Comment tenir à trois dans une boîte? Comment manipuler un volume qui fait 3 fois sa taille?</p>
<p>Entre le déménagement et l’image de l’autruche qui met sa tête dans un trou. Monter une étagère dans un escalier étroit, déplacer une poutre de 15 mètres de long à deux. Ce sont des gestes qui autant dans leur réalisation technicienne par des ouvriers en chantier que dans leur réalisation maladroite dans un déménagement entre amis sont à observer. La maladresse pour moi est une forme d’inadaptation à des éléments construits pour nous selon une norme. Cette inadaptation fait surgir une sorte de résistance ou du moins une tension, relative à chaque corps et à chaque individu. Elle a aussi une force créative qui consiste à faire autrement. Le mythe devenu expression, selon lequel l’autruche met sa tête dans un trou pour se cacher me plait. L’image est intrigante. Y aurait-il un réconfort à glisser sa tête dans un trou? Ne pas voir, est-ce une protection? Ici, les trous ouvrent sur l’extérieur et créent le lien entre un intérieur confiné intime et un extérieur. Les trous sont aussi une invitation au corps du spectateur, qu’il y réponde ou non.</p>
<br>
<img src="img/irina/irina_1.jpg"/>
<img src="img/irina/irina_2.jpg"/>


<!-- BREAK -->

<img style="margin-top: -3.3cm;" src="img/irina/irina_2.jpg"/>
<img src="img/irina/irina_7.JPG"/>
<img src="img/irina/irina_9.JPG"/>
<img src="img/irina/irina_4.jpeg"/>
<img src="img/irina/irina_5.jpeg"/>


<!-- PAGE -->
<!-- [10-11] -->

<img style="margin-top:-7cm" src="img/irina/irina_5.jpeg"/>
<img src="img/irina/irina_10.jpg"/>
<img src="img/irina/irina_12.jpg"/>
<img src="img/irina/irina_13.jpg"/>
<img src="img/irina/irina_14.jpg"/>

<!-- BREAK -->

<img style="margin-top: -1cm;" src="img/irina/irina_14.jpg"/>
<img src="img/irina/irina_15.jpg"/>
<img style="margin-bottom: 0.1cm" src="img/irina/irina_16.JPG"/>
<img style="margin-bottom: 0.1cm" src="img/irina/irina_17.JPG"/>
<img style="margin-bottom: 0.1cm" src="img/irina/irina_18.JPG"/>


<!-- PAGE -->
<!-- [12-13] -->

<img style="margin-top: -2.5cm; margin-bottom: 0.1cm" src="img/irina/irina_18.JPG"/>
<img style="margin-bottom: 0.1cm" src="img/irina/irina_19.JPG"/>
<img src="img/irina/irina_20.JPG"/>

<h2 class="type">I n s t a l l a t i o n</h2>
<h2 class="nom">Jot<span class="space">&thinsp;&thinsp;</span>Fau <span class="space">&thinsp;&thinsp;</span>Sleepy<span class="space">&thinsp;&thinsp;</span>time <span class="space">&thinsp;&thinsp;</span>has ended <br>     </h2>
<br>
<img src="img/jot/jot_2.jpg"/>
<img src="img/jot/jot_3.jpg"/>
<img src="img/jot/jot_4.jpg"/>

<!-- BREAK -->

<img style="margin-top: -2cm;" src="img/jot/jot_4.jpg"/>
<img src="img/jot/jot_5.jpeg"/>
<img src="img/jot/jot_7.JPG"/>


<!-- PAGE -->
<!-- [14-15] -->

<img style="margin-top: -3.5cm" src="img/jot/jot_7.JPG"/>
<img src="img/jot/jot_9.jpeg"/>
<img src="img/jot/jot_10.jpeg"/>
<img src="img/jot/jot_11.jpeg"/>

<!-- BREAK -->

<img style="margin-top: -1cm" src="img/jot/jot_11.jpeg"/>
<img src="img/jot/jot_14.jpeg"/>
<img src="img/jot/jot_16.jpeg"/>
<img src="img/jot/jot_18.jpeg"/>
<img src="img/jot/jot_19.jpeg"/>


<!-- PAGE -->
<!-- [16-17] -->

<img src="img/jot/jot_20.jpeg"/>
<img src="img/jot/jot_23.jpeg"/>
<img src="img/jot/jot_24.jpeg"/>
<img src="img/jot/jot_27.jpg"/>

<!-- BREAK -->
<img style="margin-top: -8cm"  src="img/jot/jot_27.jpg"/>
<img src="img/jot/jot_29.jpg"/>

<h2 class="type">c h a n t  d a n s e  a y r a n </h2>
<h2 class="nom">André D. <span class="space">&thinsp;&thinsp;</span>Chapatte <span class="space">&thinsp;&thinsp;</span>Private<span class="space">&thinsp;&thinsp;</span>utopia<br>     </h2>
<br>

<div class="poem">
	<p>SNACK ATLAS</p>
	<p>Some people have a place they’re from</p>
	<p>A place they’ve been</p>
	<p>A time or two</p>
	<br>
	<p>The Multicultural Surprise</p>
	<p>A bit of home on either side</p>
	<p>He’s got’a date tonight,</p>
	<p>She’s keepin it tight</p>
	<p>oho Ho</p> 
	<p>Where You From, Where Is Home</p>
	<br>
	<p>I’d show you if I could</p>
	<p>But you’d have to make it</p>
	<p>- </p>
	<p class="line">Back In Morocco there’s a snack bar in the sun</p>
	<p class="line">Back in Morocco there are memories of fun</p>
	<p class="line">Back in Morocco there is sunshine on the soul</p>
	<p>But back in morocco still ain’t home</p>
	<p>- </p>
	<p>Everybody owns a telephone</p>
	<p>Most times it’s 1</p>
	<p>Maybe it’s 2</p>
	<br>
</div>

<!-- PAGE -->
<!-- [18-19] -->

<div class="poem">
	<p>The world unlike it was before</p>
	<p>The city’s changed</p>
	<p>And someone’s born</p>
	<br>
	<p>Hold On / Bad Call / Redial / More Talk</p>
	<p> New lines will only cause</p>
	<p>A Multicultural Surprise</p>
	<p> And humans changing</p>
	<p>- </p>
	<p>Back In Morocco</p>
	<p>Back in Vienna</p>
	<p class="line">Back in Brussels there is sunlight on the soul</p>
</div>
<br>
<img src="img/andre/andre_2.jpg"/>
<img src="img/andre/andre_3.jpg"/>
<img src="img/andre/andre_4.jpg"/>
<img src="img/andre/andre_5.jpg"/>

<!-- BREAK -->

<img  style="margin-top: -3.5cm" src="img/andre/andre_5.jpg"/>
<img src="img/andre/andre_7.jpg"/>
<img src="img/andre/andre_10.jpg"/>
<img src="img/andre/andre_9.jpg"/>

<!-- PAGE -->
<!-- [20-21] -->

<img  style="margin-top: -5cm" src="img/andre/andre_9.jpg"/>

<h2 class="type">S c u l p t u r e s </h2>
<h2 class="nom">Stephen<span class="space">&thinsp;&thinsp;</span> Blumenschein<span class="space">&thinsp;&thinsp;</span>Passerby<span class="space">&thinsp;&thinsp;</span> (stages)  </h2>
<br>    
<br>
<h2 class="type">B a l a d e&nbsp;&nbsp;n o c t u r n e </h2>
<h2 class="nom">  Clément<span class="space">&thinsp;&thinsp;</span>Thiry<br>
Visite des coins sombres de Bruxelles + Visite des éclairages défectueux de Bruxelles</h2>
<br>
<img src="img/clement/clement_1.jpg"/>
<img src="img/clement/clement_2.jpg"/>

<!-- BREAK -->

<img src="img/clement/clement_3.jpg"/>
<img src="img/clement/clement_4.jpg"/>
<img src="img/clement/clement_5.jpg"/>

<!-- PAGE -->
<!-- [22-23] -->

<img style="margin-top: -5cm" src="img/clement/clement_5.jpg"/>
<img src="img/clement/clement_6.png"/>

<h2 class="type">C h r o n i q u e &nbsp;&nbsp;m u s i c a l e &nbsp;&nbsp;f é m i n i s t e</h2>
<h2 class="nom">  Bettina<span class="space">&thinsp;&thinsp;</span>Kjordjani<br>
Pussies grab back</h2>
<p class="first">www.youtube.com/playlist?list=PLuFi1BZHm0KXPlgodqVp9LKXDiZK2xpvf</p>
<br>
<br>
<h2 class="type">P e r f o r m a n c e &nbsp;&nbsp;c u l i n a i r e</h2>
<h2 class="nom">  Stéphanie<span class="space">&thinsp;&thinsp;</span>Becquet , Marjolein<span class="space">&thinsp;&thinsp;</span>Guldentops<span class="space">&thinsp;&thinsp;</span>& Romy <span class="space">&thinsp;&thinsp;</span>Vanderveken<br>
Kitchen session</h2>
<br>
<p class="first">
Kitchen session est une invitation faite par trois femmes à venir déguster une soupe dans leur lieu d’habitation engageant une réflexion sur le langage, la fabrication de la soupe et la relation avec le voisinage.
</p>
<br>

<div class="poem">

	<p>TOUGH ABOUT SOUP - Stéphanie Becquet</p>
	<p class="line">soup in it’s different ways of appearing</p>
	<p class="line">soup and its constantly changing (quasi abscent) meaning liable to it’s content</p>
	<p>soup as an undifined definition</p>
	<p>a quasi-something</p>
	<p class="line">soup as a covering notion for things which already have a name</p>
	<p>soup is what soup is</p>
	<p>soup is clouded</p>
	<p>soup is persistent</p>
	<p>soup can not be a coincidence</p>
	<p>soup has to be something superhuman</p>
	<br>
	<p class="line">soup on the edge between language and reality</p>
	<p class="line">because soup never points to something tangible in the actuality</p>
	<p>soup is around about something</p>
	
</div>

<!-- BREAK -->
	
<div class="poem">
	
	<p>soup keeps on turning round it’s self.</p>
	<p>soup is a master hula hooper</p>
	<br>
	<p>describing soup</p>
	<p>understanding soup</p>
	<p>observing soup</p>
	<p>revitalizing soup</p>
	<p>letting soup gain empathy</p>
	<br>
	<p>understanding soup as sheet music and</p>
	<p>interpret soup</p>
	<p>stage soup</p>
	<p>project soup</p>
	<p>believing soup again</p>
	<br>
	<p class="line">the rhythm of searching soup and constantly being startled while </p>
	<p class="line">hearing soup soup soup soup or seeing</p>
	<p class="line">soup soup soup soup soup soup apear</p>
	<br>
	<p class="line">because soup perpetually procrastinates soups meaning</p>
	<p>soup stays unsatisfied</p>
	<p>soup as a metaphor for searching...</p>
	<p>soup</p>
	<p class="line">soup as the quest of thè sense that stays unanswered</p>
	<p>soup is the search for soup</p>
	<p>soup lets us search for soup</p>
	<p>repeating soup until it becomes a game</p>
	<p class="line">repeating soup until it gains credibility</p>
	<p class="line">repeating soup until the game forgot its origin</p>
	<br>
	<p class="line">soup is played in an illusive restriction</p>
	<p class="line">soup doesn’t pretend as if it has a representative in reality,</p>
	<p class="line">meanwhile soup has everything as it’s representative.</p>
	<p class="line">Because soup can still be everything, soup is “still undefined”</p>
	<p>The border to reality seems thin</p>
	<p>because what defines what is defined?</p>
	<p class="line">what defines that soup is what we think soup is?</p>
	<p>Wat do we think soup is?</p>
	<br>
	<p class="line">the only thing left is to play with soup</p>
	<p>discovering soup’s playfield</p>
	<p class="line">to look where the border is between: pretending to be soup and there</p>
	<p class="line">where soup doesn’t know the difference</p>
	<p class="line">anymore between soup and there where</p>
	<p class="line">it started to believe in its own</p>
	<p class="line">staging</p>
</div>

<!-- PAGE -->
<!-- [24-25] -->

<div class="poem">
	
	<p>getting fascinated through the myth of the soup</p>
	<p>The myth that soup builds around its own core
	
</div>

<!--

<div class="poem">

	<p>UNE PENSÉE À PROPOS DE LA SOUPE</p>
	<br>
	<p class="line">une soupe et ses différentes manières apparaître</p>
	<p class="line">une soupe et son caractère consentement changeable (presque absent) sens</p>
	<p  class="line">assujetti sur son contenu </p>
	<p>une soupe et sa définition indéfinie</p>
	<p>une presque quelque chose</p>
	<p class="line">une soupe comme une notion couvrante pour les choses qui ont déjà un nom</p>
	<p>une soupe est ce que la soupe est</p>
	<p>une soupe est nuageuse</p>
	<p>une soupe est persistante</p>
	<p class="line">une soupe ne peux pas être une coïncidence</p>
	<p class="line">une soupe doit être quelque chose de super humain</p>
	<br>
	<p>une soupe sur la frontière</p>
	<p class="line">entre langage et réalité</p>
	<p class="line">parce que la soupe ne fait jamais remarquer quelque chose de tangible</p>
	<p class="line">dans la réalité</p>
	<p class="line">une soupe est toujours autour de quelque chose </p>
	<p>une soupe tourne autour d’elle-même</p> 

	<p class="line">une soupe est la maîtrise de l’hula hoop</p>
	<br>
	<p>décrire une soupe </p>
	<p>comprendre une soupe </p>
	<p>observer une soupe</p>
	<p>revitaliser une soupe</p>
	<p>laisser une soupe gagner l’empathie</p>
	<br>
	<p>comprendre une soupe comme une partition et </p>
	<p>interpréter une soupe</p>
	<p>faire la mise en scène d’une soupe projeter une soupe</p>
	<p>croire la</p>
	<p>la soupe </p>
	</br>
	le rythme de chercher une soupe et constamment être surpris par entendre une soupe soupe soupe ou par constater de voir apparaitre une soupe soupe soupe
	parce que, une soupe procrastine perpétuellement son sens
	la soupe reste insatisfaite
	une soupe comme métaphore pour
	une soupe
	une soupe comme la question de la sens qui reste sans réponse
	une soupe c’est la recherche d’ une soupe
	la soupe nous permet de rechercher une soupe
	répéter une soupe jusqu’à ça devient un jeu répéter une soupe en tant
	<p class="first">de voir apparaitre une soupe soupe soupe</br>
	 parce que, une soupe procrastine perpétuellement son sens</br>
	  la soupe reste insatisfaite</br>
	une soupe comme métaphore pour</br>
	 une soupe</br>
	  une soupe comme la question du sens qui reste sans réponse une soupe c’est la recherche d’ une soupe la soupe nous permet de rechercher une soupe répéter une soupe jusqu’à ça devienne un jeu répéter une soupe en tant qu'une soupe gagne sa crédibilité </br>
	  répéter une soupe jusqu’au point que le jeu ait oublié son origine.</br></br>
	une soupe ça se joue dans une restriction allusive</br>
	une soupe ne prétend pas qu'elle a un représentant dans la réalité </br>
	en même temps une soupe a tout comme représentant </br>
	parce que la soupe peut encore être tout.</br>
	 la frontière avec la réalité semble fini parce que</br>
	qui définit ce qui est défini?</br>
	 qu'est ce qui définit qu’une soupe est-ce qu’on pense que une soupe est? </br>
	 qu’est qu’on pense que c’est de la soupe?</br></br>
	 
	la seule chose qui nous reste est de jouer avec</br>
	 une soupe </br>
	 découvre son terrain de jouer. </br>
	 De découvrir ou se trouve la frontière entre: prétendre être une soupe est là ou la soupe ne connait plus</br>
	  la différence entre une soupe est là ou elle a commencé à croire à sa propre mise en scène</br></br>
	  
	être fasciné par le mythe de la soupe.</br>
	 le mythe qu’elle construit autour d'elle-même.
</div>
-->

<br>
<img src="img/kitchen/kitchen_4.jpeg"/>
<img src="img/kitchen/kitchen_5.jpeg"/>
<img src="img/kitchen/kitchen_1.jpeg"/>
<img src="img/kitchen/kitchen_6.jpeg"/>
<img src="img/kitchen/kitchen_7.jpeg"/>

<!-- BREAK -->

<img style="margin-top: -0.5cm;" src="img/kitchen/kitchen_7.jpeg"/>
<img src="img/kitchen/kitchen_8.jpeg"/>
<img src="img/kitchen/kitchen_9.jpeg"/>

 <h2 class="type">P e r f o r m a n c e &nbsp;&nbsp; a v e c&nbsp;&nbsp;  u n e &nbsp;&nbsp; g u i t a r e &nbsp;&nbsp; é l e c t r i q u e &nbsp;&nbsp;</h2>
<h2 class="nom"> Otto <span class="space">&thinsp;&thinsp;</span>Byström
Slump</h2> 
<p class="first">http://sorbusgalleria.tumblr.com/</p>
<br>
<br>
 <h2 class="type">r é c i t  &nbsp;&nbsp;m u s i c a l&nbsp;&nbsp;</h2>
 <h2 class="nom"> Antoine<span class="space">&thinsp;&thinsp;</span>Loyer<span class="space">&thinsp;&thinsp;</span>&&thinsp;&thinsp;Sandra <span class="space">&thinsp;&thinsp;</span>Naji Rituel des veilles de Bredene-Aan-Zee</h2>
<br>
<img src="img/antoine/antoine_1.jpg"/>

<!-- PAGE -->
<!-- [26-27] -->

<img  style="margin-top: -5cm;" src="img/antoine/antoine_1.jpg"/>
<img src="img/antoine/antoine_2.jpg"/>
<img src="img/antoine/antoine_6.jpg"/>

<!-- BREAK -->

<img src="img/antoine/antoine_7.jpg"/>
<img src="img/antoine/antoine_8.jpg"/>
<img src="img/antoine/antoine_15.jpg"/>

<!-- PAGE -->
<!-- [28-29] -->

<img  style="margin-top: -6.1cm;" src="img/antoine/antoine_15.jpg"/>

<!-- BREAK -->

<img  style="margin-top: -23.1cm;" src="img/antoine/antoine_15.jpg"/>


<!-- PAGE -->
<!-- [30-31] -->

<img  style="margin-top: -46.53cm;" src="img/antoine/antoine_15.jpg"/>

<h2 class="type">P e r f o r m a n c e&nbsp;&nbsp;c u l i n a i r e&nbsp;&nbsp;<br>&amp;&nbsp;&nbsp;c o n v e r s a t i o n&nbsp;&nbsp;p u b l i q u e</h2>
<h2 class="nom"> Xavier<span class="space">&thinsp;&thinsp;</span>Gorgol            Transition <br>
&amp;<span class="space">&thinsp;&thinsp;</span>Rareş<span class="space">&thinsp;&thinsp;</span>Crăiuţ</h2>
<br>
<p class="first">
	Qu’est-ce qu’une transition? Est-ce que la vie n’est pas déjà une série de transitions? Concernant les expériences physiques, nous traversons tellement de portails dans une journée: les portes, les passages piétons, les arcades, les ponts, du matin jusqu’au soir: quelle peut être sa réson(n)ance interne? Est-ce qu’une transition arrive différemment ou est-ce que l’expérience est différente, si elle est annoncée? Est-ce qu’on a le choix de se retirer ou d’abandonner dans un processus de transition?
</p>
<!--<p class="first">
	What is really a transition? Isn’t life itself a series of transitions within transitions? In term of physical experiences, we are passing through so much thresholds during the day, such as doors, crosswalks, arcades, bridges, from sunset to nightfall : what can be its inner resonance? Does a transition happen differently or is it experienced differently if it is announced? Do we even have the choice to opt out or to give up on a transition?
</p>-->
<br>
<img src="img/rares/rares_2.jpg"/>
<img src="img/rares/rares_3.jpg"/>

<!-- BREAK -->

<img  style="margin-top: -7cm;" src="img/rares/rares_3.jpg"/>
<img src="img/rares/rares_4.jpg"/>
<img src="img/rares/rares_5.JPG"/>
<img src="img/rares/rares_6.JPG"/>

<h2 class="type">B a l a d e&nbsp;&nbsp;d e&nbsp;&nbsp;b a r&nbsp;&nbsp;e n&nbsp;&nbsp;b a r&nbsp;&nbsp;e t&nbsp;&nbsp;l e c t u r e s&nbsp;&nbsp;p u b l i q u e s</h2>
<h2 class="nom">POP<span class="space">&thinsp;&thinsp;</span> Potential<span class="space">&thinsp;&thinsp;</span> Office <span class="space">&thinsp;&thinsp;</span>  POP Potential Office Project</h2>
<br>
<img src="img/pop/pop_1.jpg"/>

<!-- PAGE -->
<!-- [32-33] -->

<img src="img/pop/pop_4.jpg"/>
<img src="img/pop/pop_5.jpg"/>
<img src="img/pop/pop_6.jpg"/>

<h2 class="type">C o n c e r t&nbsp;&nbsp;d e&nbsp;&nbsp;t e c h n o&nbsp;&nbsp; r&nbsp;e x p é r i m e n t a l</h2>
<h2 class="nom">Maxime<span class="space">&thinsp;&thinsp;</span>Lacôme<span class="space">&thinsp;&thinsp;</span> </h2>
<p class="first">Maxime Lacôme joue seul ou en collectif. Ses différents projets : SSISSI EMPIRE, Le Groupe de Bamako, Le Groupe de New-York, Maximoctez, Punta del Domingo. </p>
<p>Né en 1981, il vit et travaille.</p>
<p> www.soundcloud.com/maxime-lacome</p>
<br>
<br>
<h2 class="type">M i x b r e a k c o r e</h2>
<h2 class="nom">Brique <span class="space">&thinsp;&thinsp;</span>Corps<span class="space">&thinsp;&thinsp;</span> & William <span class="space">&thinsp;&thinsp;</span>Lambeau<span class="space">&thinsp;&thinsp;</span> </h2>
<p>https://soundcloud.com/briquecorps</p>
<br>
<br>
<h2 class="type">b a l a d e</h2> 
 <h2 class="nom">Marine<span class="space">&thinsp;&thinsp;</span>  Kaiser<span class="space">&thinsp;&thinsp;</span>  & Stéphanie<span class="space">&thinsp;&thinsp;</span>Verin La ballade de Kaiser et Qasimov</h2>
 <br>
<img src="img/stephanie/affichesKQ.jpg"/>

<!-- BREAK -->

<img  style="margin-top: -2cm;" src="img/stephanie/affichesKQ.jpg"/>
<img src="img/stephanie/affichesKQ-2.jpg"/>
<img src="img/stephanie/affichesKQ-3.jpg"/>

<!-- PAGE -->
<!-- [34-35] -->

<img style="margin-top: -5cm;" src="img/stephanie/affichesKQ-3.jpg"/>
<img src="img/stephanie/affichesKQ-4.jpg"/>

<h1>LES LIEUX</h1>
<h2>Le MAGA (Avenue Jean Volders, 56)</h2>
<h2>Un lieu associatif Saint-Gillois accueillant des projets diversifiés tournés vers la jeune création, l’expérimentation et le quartier.</h2>
<br>
	<img src="img/maga/maga_1.jpg"/>
	

<!-- BREAK -->

	<img src="img/maga/maga_3.jpg"/>
	<img src="img/maga/maga_4.jpg"/>
	


<h2>CAFE WATERLOO (Chaussée de Waterloo, 217)</h2>
	<h2>Le café Waterloo se situe à barrière. Les propriétaires des lieux aiment accueillir des soirées régulièrement dans un espace aménagé à l’américaine, anachronique et décalé.</h2>
<br>
	<img src="img/waterloo/waterloo_1.jpg"/>
	<img src="img/waterloo/waterloo_4.jpg"/>
	
<!-- PAGE -->
<!-- [36-37] -->

	<img src="img/waterloo/waterloo_6.JPG"/>
	
	<h2>SNACK ATLAS (Parvis de la Trinité, 6)</h2>
	<p>Le snack Atlas est un lieu de rendez-vous connu pour se désaltérer au soleil avec un bon thé à la menthe ou pour manger à toute heure une pastilla poulet amande après une soirée.</p>
	<br>
<img src="img/atlas/atlas.JPG"/>

<!-- BREAK -->

<!-- PAGE -->
<!-- [38-39] -->

<!-- BREAK -->
	
<!-- PAGE -->
<!-- [40-41] -->
	
	<!-- BREAK -->
	
<!-- PAGE -->
<!-- [42-43] -->





