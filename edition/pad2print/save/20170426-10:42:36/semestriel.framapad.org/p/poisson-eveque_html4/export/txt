<!--sommaire-->


<!-- PAGE -->
<!-- [2-3] -->
<div class="titre">Festival Poisson—Évêque</div>
<!-- BREAK -->


<div id="edito">
<p>La création de ce festival part de l’intention de relier des moments de spectacles vivants à des formes plus discrètes de performances ou de balades dans la ville en m’intéressant à des temporalités de narrations différentes. La diversité des approches  fait la particularité de cet événement mais aussi sa complexité. Comment faire tenir une hétérogénité dans un même lieu mais aussi dans un même livre ? Comment rendre visible les débats possibles de ces rencontres ?</p>

<p>Tout au long de l’organisation de ce festival, on m’a souvent demandé pourquoi je lui avais donné ce titre, <span>Poisson-Evêque</span>. Il me semble important d’en citer la source, <span>Utopia</span> de <span>Bernadette Mayer</span>. Elle y déploie une écriture aux multiples genres en passant par le récit fictif, par la poésie, par le dialogue théâtrale ou par la chronique documentaire. L’écriture de ce livre s’est faite en collaboration avec ses amis vivants, mais aussi parmi les morts qui pour autant laissent la trace de leur écriture et de leur pensée vivante à travers la pièce de Bernadette Mayer. Dans le chapitre, Le Poisson-évêque: le débat des Utopistes, on y voit se rencontrer <span>Thomas More</span> et <span>Platon</span> au même titre que des personnages sans renommé, ne délimitant plus les époques et les réputations. Si ce n’est pas un modèle, c’est alors plutôt une inspiration à continuer un ouvrage contributif, collaboratif et inclusif dans son contenu. Dans ce livre, chaque partie a été écrite en collaboration avec les artistes et les designers, soit par le dialogue, par la relecture, la contribution de la documentation ou même les apports de textes et de références.</p>

<p>Le festival a cette ambition de relier des moments de spectacles vivants à des formes plus discrètes de performances ou de balades dans la ville. La diversité des pratiques et des approches en fait sa caractéristique mais aussi son pari. Comment faire tenir une hétérogénité dans une même salle, dans un même livre, dans un même lieu.</p>
</div>

<!-- PAGE -->
<!-- [4-5] --> 


<table>
<tr>
	<td><h1></h1></td>
	<td><h1>Conversation</h1></td>
	<td><h1>Documentation</h1></td>
	<td><h1>Restitution</h1></td>
</tr>
<tr>
	<td>Irina Favero-Longo</td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Rebecca Komforti</td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Andre D Chapatte </td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Stephen Blumenschein</td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Clément Thiry</td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Bettina Kjordjani</td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Stéphanie Becquet </td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Romy Vanderveken</td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Marjorie Guldentops</td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Otto Byström  </td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Antoine Loyer </td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Sandra Naji </td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Rares Craiut</td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Xavier Gorgol</td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>POP</td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Maxime Lacôme </td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Brique Corps & William Lambeau</td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Marine Kaiser </td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
<tr>
	<td>Stéphanie Verin</td>
	<td>5</td>
	<td>5</td>
	<td>13</td>
</tr>
	</table>
	

</div>
<!-- BREAK -->

<div id="restitution"><!--<h1>Restitution<h1>--></div>
<div id="documentation"><h1>Documentation</h1></div>
<div id="conversation"><h1>Conversation</h1></div>

<!-- PAGE -->
<!-- [6-7] -->
	<!-- BREAK -->
	<div class="scan">Bernadette Mayer, <u>Utopia</u>, Bat, future, 2016<br> extrait &#8239;<u>Le poisson-évêque&#8239;: Le débat des Utopistes</u>&#8239;</div>
	<img class="imgScan" style="width:102%"  src="img/scan/scan_2.jpg"/>
<!-- PAGE -->
	<!-- [8-9] -->
	<img class="imgScan" style="width:102%" src="img/scan/scan_1.jpg"/>
	<!-- BREAK -->
	<div class="scan">Bernadette Mayer, <u>Utopia</u>, Bat, future, 2016<br> extrait &#8239;<u>Le poisson-évêque&#8239;: Le débat des Utopistes</u>&#8239;</div>
	<img class="imgScan" style="width:102%" src="img/scan/scan_3.jpg"/>
	<!-- PAGE -->
	<!-- [10-11] -->
	<img class="imgScan" style="width:102%" src="img/scan/scan_4.jpg"/>
	<!-- BREAK -->
	<div class="scan">Bernadette Mayer, <u>Utopia</u>, Bat, future, 2016<br> extrait &#8239;<u>Le poisson-évêque&#8239;: Le débat des Utopistes</u>&#8239;</div>
	<img class="imgScan" style="width:102%" src="img/scan/scan_5.jpg"/>
	<!-- PAGE -->
	<!-- [12-13] -->
	
	<img class="imgScan" style="width:102%" src="img/scan/scan_6.jpg"/>
	
	<!-- BREAK -->
	<div class="scan">Bernadette Mayer, <u>Utopia</u>, Bat, future, 2016<br> extrait &#8239;<u>Le poisson-évêque&#8239;: Le débat des Utopistes</u>&#8239;</div>
	<img class="imgScan" style="width:102%" src="img/scan/scan_7.jpg"/>
	
	<!-- PAGE -->
	<!-- [14-15] -->
	<img class="imgScan" style="width:102%" src="img/scan/scan_8.jpg"/>
	<!-- BREAK -->
	<div class="scan">Bernadette Mayer, <u>Utopia</u>, Bat, future, 2016<br> extrait &#8239;<u>Le poisson-évêque&#8239;: Le débat des Utopistes</u>&#8239;</div>
	<img class="imgScan" style="width:102%" src="img/scan/scan_9.jpg"/>
	<!-- PAGE -->
	<!-- [16-17] -->
	
	<img style="width:102%" src="img/scan/scan_10.jpg"/>
	
	<!-- BREAK -->
	<div class="scan">Bernadette Mayer, <u>Utopia</u>, Bat, future, 2016<br> extrait &#8239;<u>Le poisson-évêque&#8239;: Le débat des Utopistes</u>&#8239;</div>
	<img class="imgScan" style="width:102%" src="img/scan/scan_11.jpg"/>
	<!-- PAGE -->
	<!-- [18-19] -->
	<img  class="imgScan" style="width:102%" src="img/scan/scan_1.jpg"/>
	
	<!-- PAGE -->
	<!-- [42-43] -->
	<!-- BREAK -->
	
	<div class="colophon">
	<h2>Les Artistes&#8239;:</h2>
	<ul>
	     <li><span>Irina Favero-Longo (France)</span><br> 
	Né en 1991 à Paris, France.</li>
	<li><span>Rebecca Komforti (France)</span><br>   Né en 1987 à Paris, France.</li>
	      <li><span>Jot fau (Belgique)</span><br>  Né en 1987 à Ypres, Belgique  http://jotfau.com/ http://jotfau.tumblr.com/</li> 
	<li><span>Andre D Chapatte (Suisse / USA)</span><br>  Né en 1988 à Genève, Suisse www.andrechapatte.com</li>
	<li><span> Stephen Blumenschein (Autriche)</span><br>   Né en 1983  à «Schärding /Inn», Autriche http://so-bel.klingt.org/sblumen/</li>
	<li><span> Clément Thiry (France) </span><br> Né le Né en 1986 à Lampaul, France</li>
	<li><span>Bettina Kjordjani (France)</span><br>  Né le &&/&&/1&&& à &&&&, France  https://www.youtube.com/playlist?list=PLuFi1BZHm0KXPlgodqVp9LKXDiZK2xpvf</li>
	<li><span> Stéphanie Becquet (Belgique) </span><br> Né en 1994 à Mechelen, Belgique</li>
	<li><span> Romy Vanderveken (Belgique)</span><br>   Né en 1994 à Leuven, Belgique https://prezi.com/dny7puaparux/untitled-2014/?webgl=0 </li>
	<li><span> Marjorie Guldentops (Belgique)</span><br>   Né en 1994 à Mechelen, Belgique https://marjoleinguldentops.tumblr.com </li>
	<li><span> Otto Byström (Finlande)</span><br>  Né en 1987 à Oulu, Finlande  http://sorbusgalleria.tumblr.com/</li>
	<li><span>Antoine Loyer (France) </span><br> Né en 1983 à Oise, France https://antoineloyer.bandcamp.com/</li>
	<li><span>Sandra Naji (France)</span><br>  Né en 1988 à Nancy, France</li>
	<li><span>Rares Craiut (Roumanie)</span><br>  Né en 1980 à Cluj-Napoca, Roumanie http://www.performingfood.com/</li>
	<li><span>Xavier Gorgol (France)</span><br>  Né en 1977 à Creutzwald, France http://xaviergorgol.blogspot.be/ http://www.dailymotion.com/video/xpl5mp_morgenglanze_creation</li>
	<li><span>POP (Potential Office Project) </span><br>http://www.potentialofficeproject.org/</li>
	<li><span>Maxime Lacôme (France) </span><br>  Né en 1981 à Suresnes, France https://soundcloud.com/maxime-lacome</li>
	     <li><span>Brique Corps & William Lambeau (France)</span><br>  Né le °°/00/1&&& à &&&&, &&&&&&& https://soundcloud.com/briquecorps</li>
	<li><span>Marine Kaiser (Suisse/France)</span><br>  Né en 1992 à Genève, Suisse</li>
	<li><span>Stéphanie Verin (France) </span><br> Né en 1988 à Orléans, Francehttp://www.atelier-bek.be/blog/www/ http://www.atelier-bek.be/</li>
	</ul>
	
	<p>Un festival organisé par Stéphanie  Verin, assistée d' Émilie Salson . Ce livre  a été mis en forme par Romain Marula & Étienne Ozeray en Html et Css, imprimé sur du papier Bouffant Arcole 90 gr/m² , &&& 150gr et &&&&&   par AJM print shop . La couverture-poster est imprimé en riso chez Ronan Deriez sur du papier Arcoprint Milk 85g . Les caractères typographiques utilisés sont Candal, Computer Modern, Cultive Mono et Nanook toutes sous licence libre.
	
	www.tiny.cc/poissoneveque </p>
	</div>
	
	
