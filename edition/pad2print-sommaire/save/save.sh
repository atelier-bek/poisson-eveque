#!/bin/bash


#!/bin/sh
while [ true ]
do

  day=$(date +%Y%m%d)
  hour=$(date +"%T")
  # interval in second (1800sec = 30min)
  interval=1800
  urlHtml1=https://semestriel.framapad.org/p/poisson-eveque_html1/export/txt
  urlHtml2=https://semestriel.framapad.org/p/poisson-eveque_html2/export/txt
  urlHtml3=https://semestriel.framapad.org/p/poisson-eveque_html3/export/txt
  urlHtml4=https://semestriel.framapad.org/p/poisson-eveque_html4/export/txt
  urlCss=https://semestriel.framapad.org/p/poisson-eveque_css/export/txt
urlCss2=http://semestriel.framapad.org/p/poisson-eveque_css2/export/txt
  urlSetup=https://semestriel.framapad.org/p/poisson-eveque_setup/export/txt

  tput setaf 1
    echo
    echo "Hi, it is $hour, I'm starting saving files"
  tput sgr0

  mkdir $day'-'$hour
  cd $day'-'$hour

  wget -E -H -k -K -p $urlHtml1 $urlHtml2 $urlHtml3 $urlHtml4 $urlCss $urlCss2 $urlSetup

  cd ..

  tput setaf 1
    echo "Job done, I'm sleeping for $interval seconds, good night."
    sleep $interval
  tput sgr0

done
