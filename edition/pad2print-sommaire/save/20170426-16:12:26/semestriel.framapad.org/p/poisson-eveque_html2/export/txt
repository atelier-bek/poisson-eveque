<!--documentation-->


<!-- PAGE -->
<!-- [6-7] -->



<h2 class="type first">P e r f o r m a n c e</h2>
<h2 class="nom">Irina Favero-Longo <br>&amp; Rebecca Konforti</h2>
<h2> Posé entre le coin et la marche</h2>
<br>
<img src="img/irina/irina_4.jpeg"/>
<img src="img/irina/irina_5.jpeg"/>

<p class="first">La forme de mon cube à trous vient d’une image mentale, une utopie dans laquelle l’humain et l’architecture fusionnent: une créature hybride entre corps humain et architecture. L’utopie subit une transformation quand elle est matérialisée et confrontée à une corporalité. Un mur à deux têtes, un cube à 4 pieds.</p>
<p>Ce sont des formes qui fonctionnent d’après une réciprocité: l’élément architectural accueille le corps et le corps donne vie à ce dernier. Les deux</p>

<!-- BREAK -->

<p> fonctionnent ensemble. Je construis alors des sculptures à trous qui seront manipulées par des corps. Elles sont structures à habiter et objets afonctionnels.</p>
<p style="margin-top:-0.1cm;">Objets lourds, gros, et inconfortables. Ces sculptures à trous sont aussi rendues «&#8239;vivantes&#8239;» par plusieurs personnes, ce qui implique un corps commun, dans lequel le rythme lent permet un déplacement fluide et une écoute de l’autre. C’est cet espace d’inconfort, celui d’une certaine gaucherie dans l’apprivoisement de ces sculptures par les corps qui pour moi crée un événement poétique.</p>
<p>Par les trous, les performeurs se montrent de manière fragmentée. Dans ces performances le corps alterne entre mouvements et postures. Entre mobilité et inertie. Dans les déplacements, c’est le corps qui agit sur la structure qu’il porte et lui donne vie, dans les pauses c’est la structure qui agit sur le corps. Le corps devient une partie de la sculpture, il se fond. Il devient un élément quasi-pictural, un ornement. On peut alors le scruter comme une œuvre. Cependant le fait qu’il s’agisse d’une sculpture humaine interroge la distance sociale&#8239;: celle que garde le spectateur, qui est une autre forme de distance que la distance face à une œuvre.</p>
<p>Cette conscience d’humanité est aussi amenée par un aspect essentiel de ces sculptures&#8239;: le double point de vue. Face à elles on voit des surfaces blanches et finies. De derrière on voit la structure intérieure, le bois brute, les éléments de construction et les corps qui tentent de la manipuler et de négocier avec. Ces deux faces</p>

<!-- PAGE -->
<!-- [8-9] -->

<p> sont toutes deux aussi importantes. Elles participent à la sculpture globale. L’envers du décor bricolé et chaotique est aussi beau que la face lisse. Sa présence est nécessaire.
Comment tenir à trois dans une boîte&#8239;? Comment manipuler un volume qui fait 3 fois sa taille&#8239;?</p>
<p>Entre le déménagement et l’image de l’autruche qui met sa tête dans un trou. Monter une étagère dans un escalier étroit, déplacer une poutre de 15 mètres de long à deux. Ce sont des gestes qui autant dans leur réalisation technicienne par des ouvriers en chantier que dans leur réalisation maladroite dans un déménagement entre amis sont à observer. La maladresse pour moi est une forme d’inadaptation à des éléments construits pour nous selon une norme. Cette inadaptation fait surgir une sorte de résistance ou du moins une tension, relative à chaque corps et à chaque individu. Elle a aussi une force créative qui consiste à faire autrement. Le mythe devenu expression, selon</p> 

<p>lequel l’autruche met sa tête dans un trou pour se cacher me plait. L’image est intrigante. Y aurait-il un réconfort à glisser sa tête dans un trou&#8239;? Ne pas voir, est-ce une protection&#8239;? Ici, les trous ouvrent sur l’extérieur et créent le lien entre un intérieur confiné intime et un extérieur. Les trous sont aussi une invitation au corps du spectateur, qu’il y réponde ou non.</p>

<img src="img/irina/irina_1.jpg"/>

<!-- BREAK -->

<img  style="margin-top: -3.5cm;" src="img/irina/irina_1.jpg"/>
<img src="img/irina/irina_2.jpg"/>
<img src="img/irina/irina_7.JPG"/>
<img src="img/irina/irina_9.JPG"/>
<img src="img/irina/irina_12.jpg"/>


<!-- PAGE -->
<!-- [10-11] -->

<img src="img/irina/irina_15.jpg"/>
<img style="margin-bottom: 0.1cm" src="img/irina/irina_16.JPG"/>
<img style="margin-bottom: 0.1cm" src="img/irina/irina_17.JPG"/>
<img style="margin-bottom: 0.1cm" src="img/irina/irina_18.JPG"/>
<img style="margin-bottom: 0.1cm" src="img/irina/irina_19.JPG"/>
<img src="img/irina/irina_20.JPG"/>

<!-- BREAK -->

<img  style="margin-top: -1.5cm;" src="img/irina/irina_20.JPG"/>

<h2 class="type">I n s t a l l a t i o n</h2>
<h2 class="nom">Jot Fau</h2>
<h2>Les fragments qui nous constituent</h2>
<br>
<p>les fragments qui nous constituent, 2017, 2m x 1m20 x 45cm, panneau en bois, couverture en laine, divers fils.</p>
<img src="img/jot/jot_2.jpg"/>
<img src="img/jot/jot_3.jpg"/>
<img src="img/jot/jot_4.jpg"/>

<!-- PAGE -->
<!-- [12-13] -->

<img style="margin-top: -5.25cm;" src="img/jot/jot_4.jpg"/>
<img src="img/jot/jot_5.jpeg"/>
<img src="img/jot/jot_7.JPG"/>


<!-- BREAK -->

<img style="margin-top: -6.75cm" src="img/jot/jot_7.JPG"/>
<img src="img/jot/jot_9.jpeg"/>
<img src="img/jot/jot_10.jpeg"/>
<img src="img/jot/jot_11.jpeg"/>


<!-- PAGE -->
<!-- [14-15] -->

<img src="img/jot/jot_14.jpeg"/>
<img src="img/jot/jot_16.jpeg"/>
<img src="img/jot/jot_18.jpeg"/>
<img src="img/jot/jot_19.jpeg"/>
<img src="img/jot/jot_20.jpeg"/>

<!-- BREAK -->

<img  style="margin-top: -2.75cm" src="img/jot/jot_20.jpeg"/>
<img src="img/jot/jot_23.jpeg"/>
<img src="img/jot/jot_24.jpeg"/>
<img src="img/jot/jot_27.jpg"/>
<img src="img/jot/jot_29.jpg"/>

<!-- PAGE -->
<!-- [16-17] -->
<img style="margin-top: -1.75cm"  src="img/jot/jot_29.jpg"/>
<img src="img/jot/jot_29.jpg"/>

<h2 class="type">c h a n t  d a n s e  a y r a n </h2>
<h2 class="nom">André D.Chapatte</h2>
<h2>Private utopia</h2>
<br>

<div class="poem">
	<p>SNACK ATLAS</p>
	<p class="line">Some people have a place they’re from</p>
	<p>A place they’ve been</p>
	<p>A time or two</p>
	<br>
	<p>The Multicultural Surprise</p>
</div>

<!-- BREAK -->

<div class="poem">
	<p>A bit of home on either side</p>
	<br>
	<p>He’s got’a date tonight,</p>
	<p>She’s keepin it tight</p>
	<p>oho Ho</p> 
	<p>Where You From, Where Is Home</p>
	<br>
	<p>I’d show you if I could</p>
	<p>But you’d have to make it</p>
	<p>- </p>
	<p class="line">Back In Morocco there’s a snack bar in the sun</p>
	<p class="line">Back in Morocco there are memories of fun</p>
	<p class="line">Back in Morocco there is sunshine on the soul</p>
	<p>But back in morocco still ain’t home</p>
	<p>- </p>
	<p>Everybody owns a telephone</p>
	<p>Most times it’s 1</p>
	<p>Maybe it’s 2</p>
	<br>
	<p>The world unlike it was before</p>
	<p>The city’s changed</p>
	<p>And someone’s born</p>
	<br>
	<p class="line">Hold On / Bad Call / Redial / More Talk</p>
	<p> New lines will only cause</p>
	<p>A Multicultural Surprise</p>
	<p> And humans changing</p>
	<p>- </p>
	<p>Back In Morocco</p>
	<p>Back in Vienna</p>
	<p class="line">Back in Brussels there is sunlight on the soul</p>

<p>And all of these places could be home </p>
-
<p>BRIDGE (Ayran is given to the audience)</p>
-
<p>Back In Brussels there's a snack bar in the sun </p>
<p>Back in Brussels there are memories of fun </p>
<p>Back in Morocco there is sunlight on the soul </p>
<p>But back in Morocco still ain't home</p>
<p>But Back In Vienna still ain't home</p>
<p>And all of these places could be home </p>
<br>
</div>
<img src="img/andre/andre_2.jpg"/>
<img src="img/andre/andre_3.jpg"/>



<!-- PAGE -->
<!-- [18-19] -->
<img  style="margin-top: -2.25cm" src="img/andre/andre_4.jpg"/>
<img src="img/andre/andre_5.jpg"/>
<img src="img/andre/andre_5.jpg"/>
<img src="img/andre/andre_7.jpg"/>
<img src="img/andre/andre_10.jpg"/>

<!-- BREAK -->

<img  style="margin-top: -0.25cm" src="img/andre/andre_10.jpg"/>
<img src="img/andre/andre_9.jpg"/>

<h2 class="type">S c u l p t u r e s </h2>
<h2 class="nom">Stephan Blumenschein</h2>
<h2>Passerby (stages)  </h2>
<br>    
<br>
<h2 class="type">B a l a d e&nbsp;&nbsp;n o c t u r n e </h2>
<h2 class="nom">  Clément Thiry</h2>
<h2>Visite des coins sombres <br>de Bruxelles</h2>
<h2>Visite des éclairages défectueux <br>de Bruxelles</h2>
<br>
<img src="img/clement/clement_1.jpg"/>


<!-- PAGE -->
<!-- [20-21] -->

<img style="margin-top: -2.5cm" src="img/clement/clement_1.jpg"/>
<img src="img/clement/clement_2.jpg"/>
<img src="img/clement/clement_3.jpg"/>
<img src="img/clement/clement_4.jpg"/>

<!-- BREAK -->

<img  style="margin-top: -5.5cm" src="img/clement/clement_4.jpg"/>
<img src="img/clement/clement_5.jpg"/>
<img src="img/clement/clement_6.png"/>

<h2 class="type">C h r o n i q u e &nbsp;&nbsp;m u s i c a l e<br>f é m i n i s t e</h2>
<h2 class="nom">  Bettina Kjordjani</h2>
<h2>Pussies grab back</h2>
<p class="first">www.youtube.com/playlist?list=PLuFi1BZHm0KXPlgodqVp9L KXDiZK2xpvf</p>

<!-- PAGE -->
<!-- [22-23] -->

<h2 class="type first">P e r f o r m a n c e &nbsp;&nbsp;c u l i n a i r e</h2>
<h2 class="nom">  Stéphanie Becquet , Marjolein Guldentops &amp; Romy Vanderveken</h2>
<h2>Kitchen session</h2>
<br>
<p class="first">
Kitchen session est une invitation faite par trois femmes à venir déguster une soupe dans leur lieu d’habitation engageant une réflexion sur le langage, la fabrication de la soupe et la relation avec le voisinage.
</p>
<br>

<div class="poem">
	<p>TOUGH ABOUT SOUP (STÉPHANIE BECQUET)</p>
	<p class="line">Soup in it’s different ways of appearing</p>
	<p>soup and its constantly changing</p> <p  class="line">(quasi abscent) meaning liable</p> <p  class="line">to it’s content</p>
	<p>soup as an undifined definition</p>
	<p>a quasi-something</p>
	<p class="line">soup as a covering notion for things which already have a name</p>
	<p>soup is what soup is</p>
	<p>soup is clouded</p>
	<p>soup is persistent</p>
	<p>soup can not be a coincidence</p>
	<p  class="line">soup has to be something superhuman</p>
	<br>
	<p class="line">soup on the edge between language and reality</p>
	<p class="line">because soup never points to something tangible in the actuality</p>
	<p>soup is around about something</p>
	<p class="line">soup keeps on turning round it’s self.</p>
	<p>soup is a master hula hooper</p>
	<br>
	<p>describing soup</p>
	<p>understanding soup</p>
	<p>observing soup</p>
	<p>revitalizing soup</p>
	<p>letting soup gain empathy</p>
	<br>
	<p class="line">understanding soup as sheet music and</p>
	<p>interpret soup</p>
	<p>stage soup</p>
	<p>project soup</p>
</div>
	
<!-- BREAK -->
	
<div class="poem">
	<p>believing soup again</p>
	<br>
	<p class="line">the rhythm of searching soup and constantly being startled while </p>
	<p class="line">hearing soup soup soup soup or seeing</p>
	<p class="line">soup soup soup soup soup soup apear</p>
	<br>
	<p class="line">because soup perpetually procrastinates soups meaning</p>
	<p>soup stays unsatisfied</p>
	<p class="line">soup as a metaphor for searching&hellip;</p>
	<p>soup</p>
	<p class="line">soup as the quest of the sense that stays unanswered</p>
	<p>soup is the search for soup</p>
	<p>soup lets us search for soup</p>
	<p class="line">repeating soup until it becomes a game</p>
	<p class="line">repeating soup until it gains credibility</p>
	<p class="line">repeating soup until the game forgot its origin</p>
	<br>
	<p class="line">soup is played in an illusive restriction</p>
	<p class="line">soup doesn’t pretend as if it has a representative in reality,</p>
	<p class="line">meanwhile soup has everything as it’s representative.</p>
	<p class="line">Because soup can still be everything, soup is “still undefined”</p>
	<p>The border to reality seems thin</p>
	<p class="line">because what defines what is defined?</p>
	<p class="line">what defines that soup is what we think soup is?</p>
	<p>Wat do we think soup is?</p>
	<br>
	<p class="line">the only thing left is to play with soup</p>
	<p>discovering soup’s playfield</p>
	<p>to look where the border is</p> <p class="line">between: pretending to be soup</p> 
	<p class="line">and there where soup doesn’t know</p>
	<p class="line"> the difference anymore between</p>
	<p class="line"> soup and there where it started </p>
	<p class="line">to believe in its own staging</p>
	
	<p>getting fascinated through the myth</p>
</div>
	

<!-- PAGE -->
<!-- [24-25] -->
	
<div class="poem">
	
	<p class="line"> of the soup</p>
	<p class="line">The myth that soup builds around its own core</p>
</div>
	
<!--

<div class="poem">

	<p>UNE PENSÉE À PROPOS DE LA SOUPE</p>
	<br>
	<p class="line">une soupe et ses différentes manières apparaître</p>
	<p class="line">une soupe et son caractère consentement changeable (presque absent) sens</p>
	<p  class="line">assujetti sur son contenu </p>
	<p>une soupe et sa définition indéfinie</p>
	<p>une presque quelque chose</p>
	<p class="line">une soupe comme une notion couvrante pour les choses qui ont déjà un nom</p>
	<p>une soupe est ce que la soupe est</p>
	<p>une soupe est nuageuse</p>
	<p>une soupe est persistante</p>
	<p class="line">une soupe ne peux pas être une coïncidence</p>
	<p class="line">une soupe doit être quelque chose de super humain</p>
	<br>
	<p>une soupe sur la frontière</p>
	<p class="line">entre langage et réalité</p>
	<p class="line">parce que la soupe ne fait jamais remarquer quelque chose de tangible</p>
	<p class="line">dans la réalité</p>
	<p class="line">une soupe est toujours autour de quelque chose </p>
	<p>une soupe tourne autour d’elle-même</p> 

	<p class="line">une soupe est la maîtrise de l’hula hoop</p>
	<br>
	<p>décrire une soupe </p>
	<p>comprendre une soupe </p>
	<p>observer une soupe</p>
	<p>revitaliser une soupe</p>
	<p>laisser une soupe gagner l’empathie</p>
	<br>
	<p>comprendre une soupe comme une partition et </p>
	<p>interpréter une soupe</p>
	<p>faire la mise en scène d’une soupe projeter une soupe</p>
	<p>croire la</p>
	<p>la soupe </p>
	</br>
	le rythme de chercher une soupe et constamment être surpris par entendre une soupe soupe soupe ou par constater de voir apparaitre une soupe soupe soupe
	parce que, une soupe procrastine perpétuellement son sens
	la soupe reste insatisfaite
	une soupe comme métaphore pour
	une soupe
	une soupe comme la question de la sens qui reste sans réponse
	une soupe c’est la recherche d’ une soupe
	la soupe nous permet de rechercher une soupe
	répéter une soupe jusqu’à ça devient un jeu répéter une soupe en tant
	<p class="first">de voir apparaitre une soupe soupe soupe</br>
	 parce que, une soupe procrastine perpétuellement son sens</br>
	  la soupe reste insatisfaite</br>
	une soupe comme métaphore pour</br>
	 une soupe</br>
	  une soupe comme la question du sens qui reste sans réponse une soupe c’est la recherche d’ une soupe la soupe nous permet de rechercher une soupe répéter une soupe jusqu’à ça devienne un jeu répéter une soupe en tant qu'une soupe gagne sa crédibilité </br>
	  répéter une soupe jusqu’au point que le jeu ait oublié son origine.</br></br>
	une soupe ça se joue dans une restriction allusive</br>
	une soupe ne prétend pas qu'elle a un représentant dans la réalité </br>
	en même temps une soupe a tout comme représentant </br>
	parce que la soupe peut encore être tout.</br>
	 la frontière avec la réalité semble fini parce que</br>
	qui définit ce qui est défini?</br>
	 qu'est ce qui définit qu’une soupe est-ce qu’on pense que une soupe est? </br>
	 qu’est qu’on pense que c’est de la soupe?</br></br>
	 
	la seule chose qui nous reste est de jouer avec</br>
	 une soupe </br>
	 découvre son terrain de jouer. </br>
	 De découvrir ou se trouve la frontière entre: prétendre être une soupe est là ou la soupe ne connait plus</br>
	  la différence entre une soupe est là ou elle a commencé à croire à sa propre mise en scène</br></br>
	  
	être fasciné par le mythe de la soupe.</br>
	 le mythe qu’elle construit autour d'elle-même.
</div>
-->
	
<br>
<img src="img/kitchen/kitchen_4.jpeg"/>
<img src="img/kitchen/kitchen_5.jpeg"/>
<img src="img/kitchen/kitchen_1.jpeg"/>
<img src="img/kitchen/kitchen_6.jpeg"/>
<img src="img/kitchen/kitchen_7.jpeg"/>

<!-- BREAK -->

<img style="margin-top: -1.5cm;" src="img/kitchen/kitchen_7.jpeg"/>
<img src="img/kitchen/kitchen_8.jpeg"/>
<img src="img/kitchen/kitchen_9.jpeg"/>

 <h2 class="type">P e r f o r m a n c e &nbsp;&nbsp; a v e c&nbsp;&nbsp;  <br>u n e &nbsp;&nbsp; g u i t a r e &nbsp;&nbsp; é l e c t r i q u e</h2>
<h2 class="nom"> Otto Byström<h2>
<h2>Slump</h2> 
<p class="first">http://sorbusgalleria.tumblr.com/</p>

<br>
<img src="img/oto/otto_1.jpg"/>
<img src="img/oto/otto_2.jpg"/>
<img src="img/oto/otto_3.jpg"/>

 <h2 class="type">r é c i t  &nbsp;&nbsp;m u s i c a l&nbsp;&nbsp;</h2>
 <h2 class="nom"> Antoine Loyer &amp; Sandra Naji</h2> 
 <h2>Rituel des veilles de Bredene-Aan-Zee</h2>
<br>
<img src="img/antoine/antoine_1.jpg"/>

<!-- PAGE -->
<!-- [26-27] -->

<img  style="margin-top: -3.5cm;" src="img/antoine/antoine_1.jpg"/>
<img src="img/antoine/antoine_2.jpg"/>
<img src="img/antoine/antoine_6.jpg"/>

<!-- BREAK -->

<img  style="margin-top: -2.5cm;" src="img/antoine/antoine_6.jpg"/>
<img src="img/antoine/antoine_7.jpg"/>
<img src="img/antoine/antoine_8.jpg"/>
<img src="img/antoine/antoine_15.jpg"/>

<!-- PAGE -->
<!-- [28-29] -->

<img  style="margin-top: -4.3cm;" src="img/antoine/antoine_15.jpg"/>

<!-- BREAK -->

<img  style="margin-top: -28cm;" src="img/antoine/antoine_15.jpg"/>

<!-- PAGE -->
<!-- [30-31] -->

<h2 class="type first">P e r f o r m a n c e&nbsp;&nbsp;c u l i n a i r e&nbsp;&nbsp;<br>&amp;&nbsp;&nbsp;c o n v e r s a t i o n&nbsp;&nbsp;p u b l i q u e</h2>
<h2 class="nom"> Xavier Gorgol &amp; Rareş Crăiuţ</h2>
<h2>Transition</h2>
<br>
<p class="first">
	Qu’est-ce qu’une transition&#8239;? Est-ce que la vie n’est pas déjà une série de transitions&#8239;? Concernant les expériences physiques, nous traversons tellement de portails dans une journée&#8239;: les portes, les passages piétons, les arcades, les ponts, du matin jusqu’au soir&#8239;: quelle peut être sa résonance interne&#8239;? Est-ce qu’une transition arrive différemment ou est-ce que l’expérience est différente, si elle est annoncée&#8239;? Est-ce qu’on a le choix de se retirer ou d’abandonner dans un processus de transition&#8239;?
</p>
<!--<p class="first">
	What is really a transition? Isn’t life itself a series of transitions within transitions? In term of physical experiences, we are passing through so much thresholds during the day, such as doors, crosswalks, arcades, bridges, from sunset to nightfall : what can be its inner resonance? Does a transition happen differently or is it experienced differently if it is announced? Do we even have the choice to opt out or to give up on a transition?
</p>-->
<img src="img/rares/rares_2.jpg"/>
<img src="img/rares/rares_3.jpg"/>

<!-- BREAK -->

<img src="img/rares/rares_4.jpg"/>
<img src="img/rares/rares_5.JPG"/>
<img src="img/rares/rares_6.JPG"/>

<h2 class="type">B a l a d e&nbsp;&nbsp;d e&nbsp;&nbsp;b a r&nbsp;&nbsp;e n&nbsp;&nbsp;b a r&nbsp;&nbsp;<br>e t&nbsp;&nbsp;l e c t u r e s&nbsp;&nbsp;p u b l i q u e s</h2>
<h2 class="nom">POP (Potential Office Project)</h2>
<br>
<img src="img/pop/pop_1.jpg"/>
<img src="img/pop/pop_4.jpg"/>

<!-- PAGE -->
<!-- [32-33] -->

<img  style="margin-top: -1.5cm;" src="img/pop/pop_4.jpg"/>
<img src="img/pop/pop_5.jpg"/>
<img src="img/pop/pop_6.jpg"/>

<h2 class="type">C o n c e r t&nbsp;&nbsp;d e&nbsp;&nbsp;t e c h n o<br>e x p é r i m e n t a l e</h2>
<h2 class="nom">Maxime Lacôme</h2>
<p class="first">Maxime Lacôme joue seul ou en collectif. Ses différents projets&#8239;: SSISSI EMPIRE, Le Groupe de Bamako, Le Groupe de New-York, Maximoctez, Punta del Domingo. </p>
<p>Né en 1981, il vit et travaille.</p>
<p> www.soundcloud.com/maxime-lacome</p>
<br>
<h2 class="type">M i x&nbsp;&nbsp;b r e a k c o r e</h2>
<h2 class="nom">Brique Corps &amp; William Lambeau</h2>
<p>https://soundcloud.com/briquecorps</p>

<!-- BREAK -->

<h2 class="type first">b a l a d e</h2> 
 <h2 class="nom">Marine Kaiser &amp; Stéphanie Verin</h2>
 <h2>La ballade de Kaiser et Qasimov</h2>
 <br>
<img src="img/stephanie/affichesKQ.jpg"/>
<img src="img/stephanie/affichesKQ-2.jpg"/>
<img src="img/stephanie/affichesKQ-3.jpg"/>


<!-- PAGE -->
<!-- [34-35] -->

<img style="margin-top: -2.5cm;" src="img/stephanie/affichesKQ-3.jpg"/>
<img src="img/stephanie/affichesKQ-4.jpg"/>

<h2 class="type">LES LIEUX</h1>
<h2>Le MAGA</h2>
<h2>Avenue Jean Volders, 56</h2>
<h2>1060 Saint-Gilles</h2>
<p>Un lieu associatif Saint-Gillois accueillant des projets diversifiés tournés vers la jeune création, l’expérimentation et le quartier.</p>
<br>
	<img src="img/maga/maga_1.jpg"/>

<!-- BREAK -->
	
	<img style="margin-top: -1cm;" src="img/maga/maga_1.jpg"/>
	<img src="img/maga/maga_3.jpg"/>
	<img src="img/maga/maga_4.jpg"/>
	

<h2 class="type"></h2>
<h2>Café Waterloo</h2>
<h2>Chaussée de Waterloo, 198</h2>
<h2>1060 Saint-Gilles</h2>

	<p>Le café Waterloo se situe à barrière. Les propriétaires des lieux aiment accueillir des soirées régulièrement dans un espace aménagé à l’américaine, anachronique et décalé.</p>
<br>
	<img src="img/waterloo/waterloo_1.jpg"/>
	
<!-- PAGE -->
<!-- [36-37] -->
	
	<img src="img/waterloo/waterloo_4.jpg"/>
	<img src="img/waterloo/waterloo_6.JPG"/>
	
	<h2 class="type"></h2>
	<h2>Snack Atlas</h2>
	<h2>Parvis de Saint-Gilles, 6</h2>
	<h2>1060 Saint-Gilles</h2>
	
	<p>Le snack Atlas est un lieu de rendez-vous connu pour se désaltérer au soleil avec un bon thé à la menthe ou pour manger à toute heure une pastilla poulet amande après une soirée.</p>
	<img src="img/atlas/atlas.JPG"/>


<!-- BREAK -->

<!-- PAGE -->
<!-- [38-39] -->

<!-- BREAK -->
	
<!-- PAGE -->
<!-- [40-41] -->
	
	<!-- BREAK -->
	
<!-- PAGE -->
<!-- [42-43] -->





