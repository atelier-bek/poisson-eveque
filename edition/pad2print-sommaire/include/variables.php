<?php

// variables globales

// ---------- instances de pad

  $projectName = 'poisson-eveque';

  $padUrl = 'https://semestriel.framapad.org/p/'.$projectName; // framapad
  // $padUrl = 'https://titanpad.com/imprimons-internet-'; // titanpad
  // $padUrl = 'http://piratepad.net/imprimons-internet_'; // piratepad

  $padHtml1 = 'https://semestriel.framapad.org/p/'.$projectName.'_html4';

  
  $padCss = 'https://semestriel.framapad.org/p/'.$projectName.'_css2';

  // ---------- pads export
  // framapad
  $padExport1 = 'https://semestriel.framapad.org/p/'.$projectName;
  $padExport2 = '/export/txt';
  // titanPad
  // $padExport1 = 'https://titanpad.com/ep/pad/export/imprimons-internet-';
  // $padExport2 = '/latest?format=txt';
  // Piratepad
  // $padExport1 = 'http://piratepad.net/ep/pad/export/imprimons-internet_';
  // $padExport2 = '/latest?format=txt';


$setup          = file_get_contents($padExport1.'_setup'.$padExport2);
$documentation  = file_get_contents($padExport1.'_doc/export/html');

$root_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
